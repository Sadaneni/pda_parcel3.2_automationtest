Feature: AcceptanceScan Feature

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button

  Scenario: Select Location
    Given the user is on Location Page
    When the user Select the Location

  Scenario: Select Indoor
    Given the user is on Location Page
    And the user Select the acceptance Location
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen

  Scenario: Select Enquiry Office and AcceptanceScan
    Given the user is on Location Page
     And the user Select the acceptance Location
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    Given the user is on Indoor Screen
    When the user selects Enquiry office
    And the user select Acceptence Scan
    Then the user able to continue AcceptenceScanScreen

  @AcceptenceScan
  Scenario: SendBarcode in Acceptance Scan
    Given the user is on Location Page
    And the user Select the acceptance Location
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    Given the user is on Indoor Screen
    When the user selects Enquiry office
    And the user select Acceptence Scan
    Then the user able to continue AcceptenceScanScreen
    Given the user in acceptance Scan
    When the user send acceptance Scan Barcode
    And the user click on Submit Button
    And the user select Acceptence Scan
    Then the user able to continue AcceptenceScanScreen
 
