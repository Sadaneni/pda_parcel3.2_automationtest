Feature: AcceptanceScanNegFlows

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
     
  Scenario Outline: Select all subprocess for EnquireyOffice
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    And the user is on Indoor Screen
    When the user selects process as "<process>"
    Then check user is displayed with many SubProcess options

    Examples: 
      | process        |
      | Enquiry Office |

  Scenario Outline: InvalidBarcode
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    And the user is on Indoor Screen
    When the user selects Enquiry office
    And the user select Acceptence Scan
    When the user sends Invalid Barcode as "<Invalid Barcode>"
    Then Capture Error message
    And click OK button
    
    Examples:
    |Invalid Barcode|
    |50269010220|

 Scenario: User able to navigate to all options from Home Screen
   Given the user is on Location Page
   And the user Select the Location
   And the user is able to continue
   Given the user is on homepage
   Then Navigate to all options present on Home screen
   
  @Logout
  Scenario: LogOut
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    Given the user is on homepage
    When the userSwie Left
    And the user tap on Logout

  @Wifi
  Scenario: WiFi Off
    Given the user is on Location Page
    And the user turn off Wifi
    And the user Select the Location
    And the user is able to continue
