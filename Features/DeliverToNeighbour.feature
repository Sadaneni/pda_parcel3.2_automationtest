Feature: Deliver To Neighbour Feature

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button

  Scenario: Select Location
    Given the user is on Location Page
    When the user Select the Location
    Then the user is able to continue

  Scenario: Select OutDoor
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    Given the user is on homepage
    When the user selects out door
    Then continue to manage out door screen

  Scenario: Enter Delivery Route
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    Then continue to manage out door screen
    Given the user is on Delivery Route Page
    When the user enters Delivery Route
    Then the user download manifest for the delivery route successfully

  Scenario: Select Outdoor Process
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    Given the user is in Manifest screen displaying pending jobs
    When the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen

  Scenario: Select Adhoc Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    Given the user is in Outdoor process screen
    When the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen

  Scenario: Scan DTN barcode WithOut Signature
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default

  Scenario: Select DTN Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page

  @DTNWithOutSignature
  Scenario: Enter NeighbourDetails Page
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    


  Scenario: Scan DTN barcode With Signature
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the Barcode with Signature
    Then the user Barcode is read by device as selected as Deliver to Customer by default

  Scenario: Select DTN Delivery with Signature
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode with Signature
    And the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page

  Scenario: Enter NeighbourDetails Page with Signature
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode with Signature
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button

  @DTNSubmitSignature
  Scenario: Submit Signature for Barcode with Signature
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode with Signature
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    


  @DifferentDeliveryRoutes
  Scenario Outline: Different Delivery Routes
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the change Route in outdoor process
    And User tap on Yes button for confirmation
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the cancel route in outdoor process
    And User tap on Yes button for confirmation
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    Then User selects Outdoor Process as "<Outdoor new Process>"
    When the user enter collection route for Adhoc delivery as "<New Collection Route>"
    Then user clicks on OK button for Adhoc collection route
    When user enter adhoc collection details
    Then user click on continue button
    Then user select submit button
    Then user select mail type to be collected
    And user click on done button
    Then the user is on pending screen
    
   Examples:
          | New Collection Route|
          |  4871231035         |
