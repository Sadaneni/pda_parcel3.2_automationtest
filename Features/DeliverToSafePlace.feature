Feature: Deliver To SafePlace Feature

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button

  Scenario: Select Location
    Given the user is on Location Page
    When the user Select the Location
    Then the user is able to continue

  Scenario: Select OutDoor
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    Given the user is on homepage
    When the user selects out door
    Then continue to manage out door screen

  Scenario: Enter Delivery Route
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    Then continue to manage out door screen
    Given the user is on Delivery Route Page
    When the user enters Delivery Route
    Then the user download manifest for the delivery route successfully

  Scenario: Select Outdoor Process
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    Given the user is in Manifest screen displaying pending jobs
    When the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen

  Scenario: Select Adhoc Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    Given the user is in Outdoor process screen
    When the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen

  Scenario: Scan DTS Barcode
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    Given the user is in Outdoor process screen
    When the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default

  Scenario: Select DTS Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page

  @DifferentBarcode
  Scenario: Scan Different Barcodes
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes
      | Barcode Names  | Description | Barcodes         |
      | DTNBARCODE     |             | QC111111115GB    |
      | DTNSIGNBARCODE |             | TP111111115GB    |
      | DTSPBARCODE    |             | QC005315222GB    |
      | RMSS           |             | BD06600000417380 |
      | RMSS           |             | BD06600000444178 |
      | RMSS           |             | BD06600000414406 |
      | RMSS           |             | BD06600000435701 |
      | PFWW           |             | BC234567895GB    |
      | PFWW           |             | BG234567895GB    |
      | PFWW           |             | BL234567895GB    |
      | PFWW           |             | CL234567895GB    |
      | PFWW           |             | DQ234567895GB    |
      | PFWW           |             | MK234567895GB    |
      | PFWW           |             | TB234567895GB    |
    Then the user tap on DTSP Barcode
    And the user is change status screen

  Scenario: Select SafePlace for Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue

  Scenario: Confirmation Of SafePlace
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place

  
  @DelivertoSafeplacewithGarage
  Scenario: Submission of Safe Location Photo
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    When the user clik on submit photo
    Then the user is on pending job screen

  Scenario: Other Safeplace
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Other
    Then the user able to click on continue

  Scenario: Other SafePlace Description
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Other
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user is on Description Screen
    When the user enters the description
    Then the user is able to Tap on take photo

  @DelivertoSafeplacewithOther
  Scenario: Other Safeplace photo validation
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Other
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user is on Description Screen
    When the user enters the description
    Then the user is able to Tap on take photo
    Given the user take the safeplace photo
    When the user clik on submit photo
    Then the user is on pending job screen

  @NoSafePlaceAvailable
  Scenario: No Safeplace Available
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Not Deliered
    And the user selects not safe
    Then the user is on Adhoc delivery page

  Scenario: Select DTS Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page

  Scenario: Select SafePlace for Delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue

  @FaultyCamera
  Scenario: Selection of faulty camera for safeplace delivery
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    When the user clicks on my camera is Faulty
    And the user is in faulty camera screen
    And the user tick the check box
    Then the user tap on confirm and submit button
