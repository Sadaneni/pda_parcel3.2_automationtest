
Feature: Exploratory Tests

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    
    
   @Locations
   Scenario: Select all locations
    Given the user is on Location Page   
    When the user selects all locations
    
   @DoProcess
  Scenario Outline: Select all subprocess for EnquireyOffice
    Given the user is on Location Page
    And the user Select the acceptance Location
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    And the user is on Indoor Screen
    When the user selects process as "<process>"
    Then check user is displayed with many DoSubProcess options

    Examples: 
      | process             |
      | DO Inward Sorting   |  

  @EnquiryProcess
  Scenario Outline: Select all subprocess for EnquireyOffice
    Given the user is on Location Page
    And the user Select the acceptance Location
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    And the user is on Indoor Screen
    When the user selects process as "<process>"
    Then check user is displayed with many SubProcess options

    Examples: 
      | process           |
      | Enquiry Office    |

  @SelectMoreOptions
   Scenario: Select all more options
    Given the user is on Location Page
    When the user Select the acceptance Location
    And the user is on homepage
    And the user selects the home option
    When the user selects all options
    
  @SelectAllChangeStatus
  Scenario: Select all change status
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Not Deliered  
    And the user selects all Not Delivered list
    
    @Settings
    Scenario: Tap on Settings Options
    Given the user is on Location Page
    When the user Select the acceptance Location
    And the user is on homepage
    And the user selects the home option
    And the user select the settings option
    And the user click on Launch Bluetooth
    Then the user click on Accessibility
    
    @AppCrashesScenario
    Scenario: Intermittent Issue
     Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    And the user Navigate back
    And the user Navigate back
    Then the user click Data lost  ok Confirmation
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    And the user Navigate back
    Then the user click Data lost  ok Confirmation
    And the user Navigate back
    Then the user click Data lost  ok Confirmation
    And the user Navigate back
    Then the user click Data lost  ok Confirmation
    And the user Navigate back
    When the user selects Indoor
    