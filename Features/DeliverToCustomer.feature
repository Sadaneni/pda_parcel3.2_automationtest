Feature: Deliver To Customer Feature

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    And the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen

  @CustomerwithoutSignature
  Scenario: Scan customer Barcode without Signature
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user verifies the Duplicate Text
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Then the user is on pending screen

 
  @CustomerwithSignature
  Scenario Outline: Scan customer Barcode with Signature
    And user enter barcode manually as "<Barcode>"
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    Then the user is on pending screen

    Examples: 
      | Barcode       |
      | RH987654326GB |
