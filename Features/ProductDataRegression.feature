Feature: ProductData Regression

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    And the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    And  the use add deviceid "354176060769866"
    
  @Customer
  Scenario: Deliver to Customer without Signature
    When the user scan the different Barcodes and event codes
      | Product Group                 | Description                                  | Barcodes      |  EventCode|
      | International Parcels         | Inward Transit Express                       | TE987654326GB |  EVKOP    |
      | International Parcels         | Inward Transit Registered/ Ins.              | TR987654326GB |  EVKOP    |
      | International Signed          | Training Unsigned                            | DB987654326GB |  EVKOP    |
      | International Signed          | Training Signed                              | MP987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UB987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UG987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UH987654326GB |  EVKOP    |
      | Royal Mail Parcels            | Local Collect                                | LC987654326GB |  EVKOP    |
      | Special Delivery Guranted 1PM | Business Mail Secure                         | CT987654326GB |  EVKOP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options                                   |
      | Deliver to Neighbour,Deliver to SafePlace |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page

  Scenario: Deliver to Customer Signature
    When the user scan the different Barcodes and event codes
      | Product Group                   | Description                              | Barcodes      |  EventCode|
      | International Signed            | International Signed                     | RH987654326GB |  EVKSP    |
      | International Signed            | International Signed For                 | RI987654326GB |  EVKSP    |
      | International Standard          | International subject to Customs Control | UJ987654326GB |  EVKSP    |
      | Parcelforce                     | International Parcel                     | CA987654326GB |  EVKSP    |
      | Special Delivery Guaranteed 1pm | Registered Plus                          | PK987654326GB |  EVKSP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options                                   |
      | Deliver to Neighbour,Deliver to SafePlace |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button

  Scenario: Deliver to Neighbour without Signature
    When the user scan the different Barcodes and event codes
      | Product Group               | Description                      | Barcodes      |  EventCode|
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Parcels       | AV987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Large Letters | AW987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Parcels       | TV987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Large Letters | TY987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Parcels       | AX987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Large Letters | AY987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Parcels       | VP987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Large Letters | VQ987654326GB |  EVKDN    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options              |
      | Deliver to SafePlace |
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen

  Scenario: Deliver to Safeplace or Deliver to Neighbour without Signature
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                                    | Barcodes      |  EventCode|
      | Royal Mail Tracked 24 | RM Tracked Nxt Day Non Signature               | ET987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 Non Signature                    | FQ987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 Non Signature                    | FR987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 High Volume non-signature        | FY987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) Non Signature | HY987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked Next Day Non Signature RDC          | JG987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) Non Signature | JQ987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 Non Signature            | JU987654326GB |  EVKOP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options
      | options                                                                                |
      | Deliver to Customer,Deliver to Safeplace,Deliver to Local Collect,Deliver to Neighbour |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page

  Scenario: Deliver to Neighbour with Signature
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                                     | Barcodes      |  EventCode|
      | Royal Mail Signed For | Signed For                                      | AI987654326GB |  EVKNS    |
      | Royal Mail Signed For | BSS Signed For                                  | BV987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | TRACKED 24 ONLINE- SIGNED FOR                   | MV987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) With Signature | QB987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) With Signature      | QD987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 With Signature            | QJ987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) With Signature      | QL987654326GB |  EVKNS    |
      | Royal Mail Tracked 48 | TRACKED 48 ONLINE - SIGNED FOR                  | YW987654326GB |  EVKNS    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options              |
      | Deliver to SafePlace |
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button

  Scenario: Deliver to All
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                        | Barcodes      |  EventCode|
      | Parcelforce           | Express by 09.00 Weekday delivery  | VW987654326GB |  EVKSP    |
      | Parcelforce           | Express by 10.00 Saturday delivery | XS987654326GB |  EVKSP    |
      | Parcelforce           | Express by 10.00 Weekday delivery  | XW987654326GB |  EVKSP    |
      | Royal Mail Signed For | Signed For                         | GQ987654326GB |  EVKSP    |
      | Royal Mail Signed For | Royal Mail Signed For              | NJ987654326GB |  EVKSP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options
      | options                                                                                |
      | Deliver to Customer,Deliver to Safeplace,Deliver to Local Collect,Deliver to Neighbour |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button

  @ProductDataRegression
  Scenario: Product Data Regression
    When the user scan the different Barcodes and event codes
      | Product Group                 | Description                                  | Barcodes      |  EventCode|
      | International Parcels         | Inward Transit Express                       | TE987654326GB |  EVKOP    |
      | International Parcels         | Inward Transit Registered/ Ins.              | TR987654326GB |  EVKOP    |
      | International Signed          | Training Unsigned                            | DB987654326GB |  EVKOP    |
      | International Signed          | Training Signed                              | MP987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UB987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UG987654326GB |  EVKOP    |
      | International Standard        | International Non Tracked Electronic Customs | UH987654326GB |  EVKOP    |
      | Royal Mail Parcels            | Local Collect                                | LC987654326GB |  EVKOP    |
      | Special Delivery Guranted 1PM | Business Mail Secure                         | CT987654326GB |  EVKOP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options                                   |
      | Deliver to Neighbour,Deliver to SafePlace |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes and event codes
      | Product Group                   | Description                              | Barcodes      |  EventCode|
      | International Signed            | International Signed                     | RH987654326GB |  EVKSP    |
      | International Signed            | International Signed For                 | RI987654326GB |  EVKSP    |
      | International Standard          | International subject to Customs Control | UJ987654326GB |  EVKSP    |
      | Parcelforce                     | International Parcel                     | CA987654326GB |  EVKSP    |
      | Special Delivery Guaranteed 1pm | Registered Plus                          | PK987654326GB |  EVKSP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options                                   |
      | Deliver to Neighbour,Deliver to SafePlace |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes and event codes
      | Product Group               | Description                      | Barcodes      |  EventCode|
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Parcels       | AV987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Large Letters | AW987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Parcels       | TV987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 24 | Doorstep Scan RM24 Large Letters | TY987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Parcels       | AX987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Large Letters | AY987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Parcels       | VP987654326GB |  EVKDN    |
      | Amazon Delivered Scanned 48 | Doorstep Scan RM24 Large Letters | VQ987654326GB |  EVKDN    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options              |
      | Deliver to SafePlace |
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                                    | Barcodes      |  EventCode|
      | Royal Mail Tracked 24 | RM Tracked Nxt Day Non Signature               | ET987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 Non Signature                    | FQ987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 Non Signature                    | FR987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked 24 High Volume non-signature        | FY987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) Non Signature | HY987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | RM Tracked Next Day Non Signature RDC          | JG987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) Non Signature | JQ987654326GB |  EVKOP    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 Non Signature            | JU987654326GB |  EVKOP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options
      | options                                                                                |
      | Deliver to Customer,Deliver to Safeplace,Deliver to Local Collect,Deliver to Neighbour |
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                                     | Barcodes      |  EventCode|
      | Royal Mail Signed For | Signed For                                      | AI987654326GB |  EVKNS    |
      | Royal Mail Signed For | BSS Signed For                                  | BV987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | TRACKED 24 ONLINE- SIGNED FOR                   | MV987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) (HV) With Signature | QB987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) With Signature      | QD987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 With Signature            | QJ987654326GB |  EVKNS    |
      | Royal Mail Tracked 24 | Royal Mail Tracked 24 (LBT) With Signature      | QL987654326GB |  EVKNS    |
      | Royal Mail Tracked 48 | TRACKED 48 ONLINE - SIGNED FOR                  | YW987654326GB |  EVKNS    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options              |
      | Deliver to SafePlace |
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes and event codes
      | Product Group         | Description                        | Barcodes      |  EventCode|
      | Parcelforce           | Express by 09.00 Weekday delivery  | VW987654326GB |  EVKSP    |
      | Parcelforce           | Express by 10.00 Saturday delivery | XS987654326GB |  EVKSP    |
      | Parcelforce           | Express by 10.00 Weekday delivery  | XW987654326GB |  EVKSP    |
    #  | Royal Mail Signed For | Signed For                         | GQ987654326GB |  EVKSP    |
    #  | Royal Mail Signed For | Royal Mail Signed For              | NJ987654326GB |  EVKSP    |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    Then user fetch all barcode options and verify barcode options
      | options                                                                                |
      | Deliver to Customer,Deliver to Safeplace,Deliver to Local Collect,Deliver to Neighbour |
    And the user selects the Deliver to SafePlace
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    When the user clik on submit photo
    Then the user is on pending job screen
