Feature: PDA Product Data

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    And the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen

  Scenario: Product Data DTN without Signature
    When the user scan the different Barcodes
      | Barcode Names   | Description                   | Barcodes      |
      | AmazonDelivered | DoorStepScanRM24 LargeLetters | AW111111115GB |
      | AmazonDelivered | DoorStepScanRM24 Parcels      | TV111111115GB |
      | AmazonDelivered | DoorStepScanRM24 LargeLetters | TY111111115GB |
      | RMTracked       | RM Tracked High volume        | FF111111115GB |
      | RMTracked       | RM Tracked Non Signature      | FE111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen

  Scenario: Product Data DTN with Signature
     When the user scan the different Barcodes
      | Barcode Names      | Description          | Barcodes      |
      | RMSignedFirstClass | Signed For           | AG111111115GB |
      | RMSigned           | Signed For           | BR111111115GB |
      | Parcelforce        | International Parcel | CK111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button

  Scenario: Product Data DTC with Signature
      When the user scan the different Barcodes
      | Barcode Names     | Description               | Barcodes      |
      | SPD guarented 1pm | Special Delivery Next Day | AE111111115GB |
      | Parcelforce       | Parcelforce Only          | CU111111115GB |
      | Parcelforce       |                           | GA111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button

  @ProductData
  Scenario: Product Data DTS with photo
      When the user scan the different Barcodes
      | Barcode Names | Description                          | Barcodes      |
      | Parcelforce   | International Parcel                 | CE111111115GB |
      | RM Tracked    | RM Tracked+Non Signature Presorated  | FL111111115GB |
      | RM Tracked    | RM Tracked+No Signature No Sortation | FJ111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user want apply all of items
    And the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    When the user clik on submit photo
    Then the user is on pending job screen

  @RegressionProductData
  Scenario: Product Data
     When the user scan the different Barcodes
      | Barcode Names   | Description                   | Barcodes      |
      | AmazonDelivered | DoorStepScanRM24 LargeLetters | AW111111115GB |
      | AmazonDelivered | DoorStepScanRM24 Parcels      | TV111111115GB |
      | AmazonDelivered | DoorStepScanRM24 LargeLetters | TY111111115GB |
      | RMTracked       | RM Tracked High volume        | FF111111115GB |
      | RMTracked       | RM Tracked Non Signature      | FE111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes
      | Barcode Names      | Description          | Barcodes      |
      | RMSignedFirstClass | Signed For           | AG111111115GB |
      | RMSigned           | Signed For           | BR111111115GB |
      | Parcelforce        | International Parcel | CK111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user want apply all of items
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes
      | Barcode Names     | Description               | Barcodes      |
      | SPD guarented 1pm | Special Delivery Next Day | AE111111115GB |
      | Parcelforce       | Parcelforce Only          | CU111111115GB |
      | Parcelforce       |                           | GA111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to customer
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    When the user scan the different Barcodes
      | Barcode Names | Description                          | Barcodes      |
      | Parcelforce   | International Parcel                 | CE111111115GB |
      | RM Tracked    | RM Tracked+Non Signature Presorated  | FL111111115GB |
      | RM Tracked    | RM Tracked+No Signature No Sortation | FJ111111115GB |
    Then the user tap on DTSP Barcode
    And the user is change status screen
    And the user selects the Deliver to SafePlace
    Then the user want apply all of items
    And the user is on Adhoc delivery page
    Given the user is on choose SafePlace screen
    When the user selects the Garage
    Then the user able to click on continue
    When the user is on SafePlace location request window
    Then the user clicks on confirm location is safe Taken photo of item in safe place
    Given the user take the safeplace photo
    When the user clik on submit photo
    Then the user is on pending job screen
