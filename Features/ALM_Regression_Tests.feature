Feature: ALM_Regression_Tests

  Background: 
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    When the user Select the Location
    Then the user is able to continue
    Given the user is on homepage
    When the user selects out door
    Then continue to manage out door screen

  #Different Delivery & Collection Routes
  @Yes&CancelBtn
  Scenario Outline: Regression TC 96,95 - Collection & Delivery Route - Different
    Given the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    When user selects the job from the list
    And the user scan the Barcode as "<Barcode>"
    And click on submit button in JobDetails page
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User tap on Cancel button in Closing Outdoor
    And User should stay in Outdoor process screen
    When User selects the Outdoor Process dropdown
    And the user selects the Adhoc Delivery in outdoor process
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    And User tap on Yes button for confirmation
    And User should select the reason dropdown
    And User should select reason value as "<Reason_value_ForYesbtn>"
    Then user click on Submit button
    And the user is on homepage

    Examples: 
      | Delivery Route | Collection Route | Barcode   | Outdoor Process | Reason_value_ForYesbtn |
      |     0000000000 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue          |

  # |     4871231035 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue          |
  #Different Delivery & Collection Routes
  @NoBtn
  Scenario Outline: Regression TC 94 - Collection & Delivery Route - Different
    Given the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    When user selects the job from the list
    And the user scan the Barcode as "<Barcode>"
    And click on submit button in JobDetails page
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User tap on No button in Closing Outdoor
    And User should select the reason dropdown
    And User should select reason value as "<Reason_value_ForNobtn>"
    Then user click on Submit button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    And the user selects the Adhoc Delivery in outdoor process
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User checks message on Closing Outdoor Pop up
      | message                       |
      | Outdoor finished successfully |
    Then User click on OK button on Closing Outdoor window
    And the user is on homepage

    Examples: 
      | Delivery Route | Collection Route | Barcode   | Outdoor Process | Reason_value_ForNobtn |
      |     0000000000 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue         |

  #   |     4871231035 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue         |
  #Start with Same Delivery & Collection Routes and perform Adhoc collection usign same/different collection route
  @SameRoute_N0
  Scenario Outline: Regression TC 93,92 - Collection & Delivery Route - Same
    Given the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    And the user clicks on Manage Outdoor button
    And the user selects the Adhoc Delivery in outdoor process
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    When user selects the job from the list
    And the user scan the Barcode as "<Barcode>"
    And click on submit button in JobDetails page
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User tap on No button in Closing Outdoor
    And User should select the reason dropdown
    And User should select reason value as "<Reason_value_ForYesbtn>"
    Then user click on Submit button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor new Process>"
    When the user enter collection route for Adhoc delivery as "<New Collection Route>"
    Then user clicks on OK button for Adhoc collection route
    When user enter adhoc collection details
    Then user click on continue button
    Then user select submit button
    Then user select mail type to be collected
    And user click on done button
    Then the user is on pending screen

    Examples: 
      | Delivery Route | Collection Route | Barcode   | Outdoor Process | Reason_value_ForYesbtn | Outdoor new Process | New Collection Route |
      |     4871231035 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue          | Adhoc Collection    |           4871239038 |
      |     4871231035 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue          | Adhoc Collection    |           4871231035 |

  #Start with Same Delivery & Collection Routes and perform Adhoc collection job usign same collection route
  @SameRoute_Yes
  Scenario Outline: Regression TC 91 - Collection & Delivery Route - Same
    Given the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    And the user clicks on Manage Outdoor button
    And the user selects the Adhoc Delivery in outdoor process
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    When user selects the job from the list
    And the user scan the Barcode as "<Barcode>"
    And click on submit button in JobDetails page
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User tap on No button in Closing Outdoor
    And User should select the reason dropdown
    And User should select reason value as "<Reason_value_ForYesbtn>"
    Then user click on Submit button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor new Process>"
    When the user enter collection route for Adhoc delivery as "<New Collection Route>"
    Then user clicks on OK button for Adhoc collection route
    When user enter adhoc collection details
    Then user click on continue button
    Then user select submit button
    Then user select mail type to be collected
    And user click on done button
    Then the user is on pending screen
    And the user clicks on Manage Outdoor button
    Then User selects Outdoor Process as "<Outdoor Process>"
    Then User tap on Yes button for confirmation
    # And User should select the reason dropdown
    # And User should select reason value as "<Reason_value_ForYesbtn>"
    # Then user click on Submit button
    And the user is on homepage

    Examples: 
      | Delivery Route | Collection Route | Barcode   | Outdoor Process | Reason_value_ForYesbtn | Outdoor new Process | New Collection Route |
      |     4871231035 |       4871231035 | BOX000002 | Finish Outdoor  | Barcode Issue          | Adhoc Collection    |           4871231035 |

  @UnmanifestedTypes
  Scenario Outline: Regression TC 64 - Scan an unmanifested BOX Wall plate
    Given the user is on Delivery Route Page
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    When the user scan the Barcode as "<Barcode>"
    Then user verifies adhoc collection popup text and click OK button
    Then user select submit button
    Then the user is on pending screen

    Examples: 
      | Collection Route | Barcode   |
      |       4871231035 | BOX564789 |

  @UnmanifestedTypes1
  Scenario Outline: Regression TC 65,66 - Scan an unmanifested FIRM,POC Wall plate
    Given the user is on Delivery Route Page
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    When the user scan the Barcode as "<Barcode>"
    Then user verifies adhoc collection popup text and click OK button
    Then user select submit button
    Then user select mail type to be collected
    And user click on done button
    Then the user is on pending screen

    Examples: 
      | Collection Route | Barcode   |
      |       4871239038 | POC564789 |

  #    | 4871240039​       | FIR564789 |
  @FIRMPPI
  Scenario Outline: Regression TC 24 - Collection Jobs_FIRM_PPI - MPC barcode
    Given the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user checks for collection route got successfully downloaded
    When user selects the job from the list
    And the user scan the Barcode as "<Barcode>"
    And click on submit button in JobDetails page

    Examples: 
      | Delivery Route | Collection Route | Barcode   |
      |     0000000000 |       4871231035 | FIR564789 |

  @ALMREGTC34
  Scenario: ALM TC 34 - Deliver to Local Collect with Signatuire
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    Given the user is in Adhoc Delivery Screen
    And the user scan the Barcode with Signature
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the option Deliver to Local Collect
    Then the user is on Adhoc delivery page
    Then the user enter customer name
    When the user tap on Done button
    Given the user is in signature Acknowledgement Page
    When the user sign the signature
    Then click on submit Button
