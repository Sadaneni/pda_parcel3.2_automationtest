Feature: UAT Smoke Tests


#  Scenario Outline: UAT Smoke Test Scenario with no 1
#    Given the user enters in with barcode username as "<barcode username>"
#    When the user enter in with barcode password
#    Then the user click on submit button
#    Given the user is on Location Page
#    And the user Select the Location
#    And the user is able to continue
#    Given the user is on homepage
#    When user perform swipe
#    Then the user tap on Logout

#    Examples: 
#     | barcode username |
#      | anna.dignam      |

 @Execution
  Scenario Outline: UAT Smoke tests with no 2 till 8
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    Then user verifies Home screen options "<home screen options>"
    When the user selects Indoor
    Then continue to Indoor screen
    And Verify the list of options under Process tab as "<Process list options>"

    Examples: 
      | username         | password | location                     | home screen options                                            | Process list options                                                                                                                                                       |
      | iain.calder4     |     1234 | Scottish Distribution Centre | INDOOR,WATCH&WIN,NAVIGATION,PHONE,MEMO,SETTINGS,LOGOUT         | Standard RDC Outward Primary,Standard RDC Outward Secondary,Standard RDC Inward Primary,Tracked RDC Outward,Tracked RDC Inward                                             |
      | ken.schiller2    |     1234 | Aberdeen Mail Centre         | INDOOR,OUTDOOR,WATCH&WIN,NAVIGATION,PHONE,MEMO,SETTINGS,LOGOUT | Standard MC Outward Primary,Standard MC Outward Secondary,Standard MC Inward Primary,Tracked MC Outward,Tracked MC Inward,Acceptance Scan                                  |
      | andrew.scarlett3 |     1234 | Acton DO                     | INDOOR,OUTDOOR,WATCH&WIN,NAVIGATION,PHONE,MEMO,SETTINGS,LOGOUT | DO Inward Sorting,Enquiry Office                                                                                                                                           |

 
  Scenario Outline: UAT Smoke Tests with no. 9,10
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process>"
    Then Verify the list of options under SubProcess tab as "<Sub Process Options>"

    Examples: 
      | username         | password | location | process           | Sub Process Options                                                                                         |
      | andrew.scarlett3 |     1234 | Acton DO | DO Inward Sorting | Ready for Delivery,Missorted Item,Damaged Item,Tracked Returns                                              |
      | andrew.scarlett3 |     1234 | Acton DO | Enquiry Office    | Ready for Delivery,Customer Collected,Record Not Delivered Item,Damaged Item,Failed Scanner,Acceptance Scan |

 
  Scenario Outline: UAT Smoke Tests with no. 27,28 - Deliver a letter to Customer with signature & Validate Empty Manifest
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And user enter barcode manually as "<Barcode>"
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    When the user is able to perform signature
    Then click on Submit button

    Examples: 
      | Barcode       |
      | tp111111115GB |

  #|jw009501738GB|
  #|0B0368194000101101077|

  Scenario Outline: UAT Smoke Tests with no. 27,28 - Deliver a letter to Customer without signature
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    # And the user scan the Barcode
    And user enter barcode manually as "<Barcode>"
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen

    Examples: 
      | Barcode       |
      | qc111111115gb |


  Scenario: Select DTS & DTLC Delivery - UAT Smoke Scenarios with no. 32,33
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Deliver to SafePlace
    Then now the device will have Deliver to Safeplace
    And the user selects the option Deliver to Local Collect

  Scenario: No Safeplace Available - UAT Smoke scenario with no. 35
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    And the user scan the Barcode
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Given the user is change status screen
    When the user selects the Not Deliered
    And the user selects not safe
    Then the user is on Adhoc delivery page

  # @HomeScreenOption
  
  Scenario: UAT scenarios with no. 37,38,41 - Navigate to Home screen options
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    Then the user selects Home screen option as Watch&Win, Phone & Navigation

  #@Memo
 
  Scenario Outline: UAT scenario
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects Home screen option as "<Home Screen Option>"
    Then User is on Memo Page
    Then user verifies Memo message as "<memo message>"

    Examples: 
      | Home Screen Option | message |
      | MEMO               | test    |

  #@DemoSig
  #Scenario: Offline mode
  # Given when the user switch off wifi
  #@ALMReg
   
  Scenario Outline: RM - ALM Regression scenario with TC 81,82,83
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process>"
    Then user select sub process value as  "<Sub Process Value>"
    Then user selects customer dropdown
    Then user selects a customer Value as "<Customer Value>"
    And the user scan the Barcode
    Then user selects customer dropdown
    Then user selects a customer Value as "<New Customer Value>"
    Then user verify the scanned items are disappeared

    Examples: 
      | username    | password | location            | process           | Sub Process Value | Customer Value         | New Customer Value   |
      | tma.test4   |     1234 | Enfield DO          | DO Inward Sorting | Tracked Returns   | People Tree Ltd 2C1675 | Me & Em 2c1808       |
      #   | iain.calder4 |     1234 | Scottish Distribution Centre | Tracked RDC Inward | Tracked Returns   | All Saint Ret Ltd 2C1101 | Phase 8 2C436      | --- username and password not present
      | Anna.dignam |     7861 | Swindon Mail Centre | Tracked MC Inward | Tracked Returns   | KEW 159 LTD 2c505      | SCOTTS LIMITED 2c511 |


  #Examples:
  # | username     | password | barcode username |
  # | iain.calder4 |     1234 | anna.dignam      |
 
  Scenario Outline: RM - ALM Regression scenario with TC 13
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects out door
    Then the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
    Then user clicks on continue button if route is not downloaded
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process>"
    Then user select sub process value as  "<Sub Process Value>"
    Then the user scan the Barcode as "<Barcode>"
    Then the user is on Adhoc delivery page
    Then the user enter customer name
    When the user tap on Done button
    When the user is able to perform signature
    Then click on Submit button
    When the user selects process as "<process>"
    Then user select sub process value as  "<Sub Process Value>"
    And user navigates back from current screen
    And user navigates back from current screen
    Then the user is on homepage
    When the user selects out door
    And the user clicks on Manage Outdoor button
    And the user selects the Adhoc Delivery in outdoor process
    Then the user continue to adhoc delivery screen
    And the user scan the Barcode as "<Barcode1>"
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    And the user is change status screen
    And the user selects the Deliver to Neighbour
    Then the user is on Adhoc delivery page
    Given the user is in neighbour detilas page
    And the user enters Neighbourname
    And the user enters House no
    And the user enter the street name
    When the user tap on Done button
    Then the user is on pending screen
    Then user navigates back from current screen
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process1>"
    Then user select sub process value as  "<Sub Process Value1>"
    Then the user scan the Barcode as "<Barcode>"
    And the user click on Submit Button
    Then user navigates back from screen
    And user navigates back from current screen
    When the user selects out door
    And the user clicks on Manage Outdoor button
    And the user selects the Adhoc Delivery in outdoor process
    Then the user scan the Barcode as "<Barcode1>"
    Then the user is on Adhoc delivery page
    #Then the user enter customer name
    #When the user tap on Done button
    #When the user is able to perform signature
    #Then click on Submit button
    Then the user is on pending screen

    Examples: 
      | username      | password | location             | home screen options                                            | Delivery Route | Collection Route | process        | Sub Process Value  | Barcode       | Barcode1      | process1          | Sub Process Value1 |
      | ken.schiller2 |     1234 | Aberdeen Mail Centre | INDOOR,OUTDOOR,WATCH&WIN,NAVIGATION,PHONE,MEMO,SETTINGS,LOGOUT |     0000000000 |       4871231035 | Enquiry Office | Customer Collected | TP111111115GB | qc111111115gb | DO Inward Sorting | Ready for Delivery |

  
  Scenario Outline: RM - ALM Regression scenario with TC 14 & 15
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process>"
    Then user select sub process value as  "<Sub Process Value>"
    And user enter barcode manually as "<Barcode>"
    And the user click on Submit Button

    Examples: 
      | username      | password | location             | process                     | Sub Process Value | Barcode               |
      | ken.schiller2 |     1234 | Aberdeen Mail Centre | Tracked MC Inward           | Accepted          | QC111111115GB         |
      | ken.schiller2 |     1234 | Aberdeen Mail Centre | Standard MC Outward Primary | Regular           | 0B0368194000101101077 |

  
  Scenario Outline: RM - ALM Regression scenario with TC 16 & 17
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects Indoor
    Then continue to Indoor screen
    When the user selects process as "<process>"
    Then user select sub process value as  "<Sub Process Value>"
    And user enter barcode manually as "<Barcode>"
    And the user click on Submit Button

    Examples: 
      | username     | password | location                     | process                        | Sub Process Value | Barcode               |
      | iain.calder4 |     1234 | Scottish Distribution Centre | Tracked RDC Inward             | Damaged           | QC111111115GB         |
      | iain.calder4 |     1234 | Scottish Distribution Centre | Standard RDC Outward Secondary | Missort           | 0B0368194000101101077 |

   @Ramana 
  Scenario Outline: RM - ALM Regression scenario with TC 19 & 20
    Given the user enters in with username as "<username>" and password as "<password>"
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location as "<location>"
    And the user is able to continue
    Given the user is on homepage
    When the user selects out door
    Then the user is on Delivery Route Page
    When the user enters Delivery Route as "<Delivery Route>"
    And the user enters Collection Route as "<Collection Route>"
    Then verify route is successfully downloaded
 #   Then Verify popup message after clicking Download Route

    Examples: 
      | username         | password | location | Delivery Route | Collection Route |
      #  | iain.calder4 |     1234 | Scottish Distribution Centre |     0000000000 |       4871231035 |
      | andrew.scarlett3 |     1234 | Acton DO |     0000000005 |       1234567890 |

  @ProductData
  Scenario Outline: Barcode Otions
    Given the user enters in with username
    When the user enter in with password
    Then the user click on submit button
    Given the user is on Location Page
    And the user Select the Location
    And the user is able to continue
    And the user is on homepage
    When the user selects out door
    And continue to manage out door screen
    And the user is on Delivery Route Page
    And the user enters Delivery Route
    Then the user download manifest for the delivery route successfully
    And the user is in Manifest screen displaying pending jobs
    And the user clicks on Manage Outdoor button
    Then the user continue to adhoc delivery screen
    And the user is in Outdoor process screen
    And the user selects the Adhoc Delivery in outdoor process
    Then the user is on AdhocDelivery screen
    And the user is in Adhoc Delivery Screen
    Then the user scan the Barcode as "<Barcode>"
    Then the user Barcode is read by device as selected as Deliver to Customer by default
    Then user fetch all barcode options and verify barcode options does not inlcude options
      | options                                   |
      | Deliver to Neighbour,Deliver to SafePlace |
    And the user selects the option Deliver to Local Collect

    Examples: 
      | Barcode       |
      | TE987654326GB |
      
