package com.automation.pages;


import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;


public class PendingScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/btManageManifest")
	private MobileElement manage;

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private MobileElement pending;
	
	@AndroidFindBy(id="android:id/home")
	private MobileElement back;
	
	//@AndroidFindBy(id="com.royalmail.pda:id/llSingnalWindowRm")
	//private List<MobileElement> selectjob;
	
	@AndroidFindBy(id="com.royalmail.pda:id/llSingnalWindowRm")
	private List<MobileElement> selectjob;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_message")
	private MobileElement Adhoc_Collection_popup_message;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement Adhoc_Collection_popup_message_OKbtn;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn2")
	private MobileElement Adhoc_Collection_popup_message_Cancelbtn;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_message")
    private MobileElement Download_Collectionroute_popup_message;
	
	
	public  PendingScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
   
	public Boolean assert_pending(){
		Boolean ispending= isElementVisible(pending,driver);
		return ispending;
	}
	public OutDoorScreen clickManageOutDoor(){
		manage.click();
		return new OutDoorScreen(driver);
	}
	

	public void selectJob(int index) throws InterruptedException{
		selectjob.get(index).click();
		Thread.sleep(3000);
	}
	public void continueToAdhoc() throws InterruptedException{
		Thread.sleep(1000);
	}
	public void clickPendingJobs(){
		pending.click();
	}
	
	public void navigateBack(){
		back.click();
	}
	
	public void verify_AdhocCollection_popuptext()
	{
		String txt = Adhoc_Collection_popup_message.getText();
		Assert.assertEquals("Collection barcode not found. Do you want to create a new collection job ?", txt);		
	}
	
	
	public void verify_DownloadCollectionroute_popuptext() throws InterruptedException
    {
        Thread.sleep(7000);
        String txt = Download_Collectionroute_popup_message.getText();
        Assert.assertTrue(txt.contains("Manifest could not be"));        
        
        
        //Assert.assertEquals("Manifest could not be downloaded.Do you want to Cancel or Retry or Continue without manifest?", txt);        
    }
	
	public void clickOK_AdhocCollectio_Popup()
	{
		Adhoc_Collection_popup_message_OKbtn.click();
	}
	
}
