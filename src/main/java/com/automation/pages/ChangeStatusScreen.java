package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import static org.assertj.core.api.Assertions.*;


public class ChangeStatusScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/groupHeader")
	private List<MobileElement> groupHeader;
	
	@AndroidFindBy(className="android.widget.TextView")
	private List<MobileElement> neighbour;

	@AndroidFindBy(id="com.royalmail.pda:id/staus_title")
	private MobileElement changeStatusText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/statusItem")
    private List<MobileElement> changeStatusList;
	
	public ChangeStatusScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	 public boolean isDuplicateFound(String textToFindDuplicatesOf){
			List<MobileElement> elements = driver.findElementsById("com.royalmail.pda:id/statusItem");
	        return elements.size() > 1;
		}
	
	public Boolean assert_changeStatusText(){
		Boolean ischangeStatusText= isElementVisible(changeStatusText,driver);
		return ischangeStatusText;
	}
	
	
	public AdhocDeliveryScreen selectDelivertoPlace(int index){
		neighbour.get(index).click();
		return new AdhocDeliveryScreen(driver);
	}
	
	public void CapturebarcodeOptions(String arg1)
    {
        String barcodeoptns = null;
        List<MobileElement> sbprcs = changeStatusList;
        int sbprcslst = sbprcs.size();
        for (int s = 0; s < sbprcslst; s++)
        {
            String sbprcsnm = sbprcs.get(s).getText();
            barcodeoptns = barcodeoptns + "," + sbprcsnm;        
        }            
        
        barcodeoptns = barcodeoptns.replace("null,", "");
        System.out.println("The values under barcode are:" + barcodeoptns);        
        assertThat(barcodeoptns).doesNotContain(arg1);
    }
	
	public void CapturebarcodeOptionsandAssert(String arg1)
    {
        String barcodeoptns = null;
        List<MobileElement> sbprcs = changeStatusList;
        int sbprcslst = sbprcs.size();
        for (int s = 0; s < sbprcslst; s++)
        {
            String sbprcsnm = sbprcs.get(s).getText();
            barcodeoptns = barcodeoptns + "," + sbprcsnm;        
        }            
        
        barcodeoptns = barcodeoptns.replace("null,", "");
        System.out.println("The values under barcode are:" + barcodeoptns);        
        assertThat(barcodeoptns).contains(arg1);
    }
	
	public void doSomething(String val) throws InterruptedException{
		for (MobileElement vd:neighbour){
			if(vd.getText().equals(val)){
				vd.click();
				break;
			}
		}
	}
	
	public void selectAllNotDeliveredList() throws InterruptedException{
		List<MobileElement> ndlist = changeStatusList;
		int ndsublst = ndlist .size();
		for (int j = 0; j < ndsublst; j++) 
		{
			String sbprsnme = ndlist.get(j).getText();
			ndlist .get(j).click();
			AdhocDeliveryScreen adhoc = new AdhocDeliveryScreen(driver);
			adhoc.clickArrow();
			ChangeStatusScreen change = new ChangeStatusScreen(driver);
			change.doSomething("Not Delivered");
	      }
       }
}


