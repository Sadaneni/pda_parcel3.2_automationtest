package com.automation.pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class IndoorProcessScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerDroped")
	private List<MobileElement> processIndex;
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private MobileElement processLayer;
	
	public IndoorProcessScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
   public void selectProcessIndex(int index){
	   processIndex.get(index).click();
   }
   
   public IndoorScreen selectProcessByText(String ProcessName){
       for (MobileElement Pro :
    	   processIndex) {
           if (Pro.getText().contains(ProcessName)) {
               Pro.click();
               break;
           }
       }
       return new IndoorScreen(driver);
   }
   
 /*  public void selectprocess(String ar1)
   {
		int indx=1;
		List<MobileElement> prcs = processIndex;
		int prcslst = prcs.size();
		for (int s = 0; s < prcslst; s++)
		{
			String prcsnm = prcs.get(s).getText();
			if(prcsnm.equals("Enquiry Office"))
			{		
				indx = s;
				break;
			}
		}
		prcs.get(indx).click();
	}
	*/
   public void selectprocess(String arg1)
   {
         int indx=1;
         List<MobileElement> prcs = processIndex;
         int prcslst = prcs.size();
         for (int s = 0; s < prcslst; s++)
         {
             String prcsnm = prcs.get(s).getText();
             if(prcsnm.equals(arg1))
             {        
                 indx = s;
                 break;
             }
         }
         prcs.get(indx).click();
     }

  
   public String CaptureProcessList()
   {
        String prcslstopt = null;
        List<MobileElement> prcs = processIndex;
         int prcslst = prcs.size();
         for (int s = 0; s < prcslst; s++)
         {
             String prcsnm = prcs.get(s).getText();
             prcslstopt = prcslstopt + "," + prcsnm;
         }
         //System.out.println("The Values under process are :" + prcslstopt);
         driver.navigate().back();
         driver.navigate().back();
         HomePageScreen scrn = new HomePageScreen(driver);
         Assert.assertTrue(scrn.assert_homeText());
         
         prcslstopt = prcslstopt.replace("null,", "");
         return prcslstopt;
   }
   
   
   public void selectProcess() throws InterruptedException, IOException
	{
		List<MobileElement> sbprcs = processIndex;
		int sbprcslst = sbprcs.size();
		for (int j = 0; j < sbprcslst; j++) 
		{
			String sbprsnme = sbprcs.get(j).getText();
			sbprcs.get(j).click();
			GenericMethods gm = new GenericMethods(driver);
			gm.CaptureScreenshot(sbprsnme);			
			EnquiryOfficeScreen clk = new EnquiryOfficeScreen(driver);
			clk.sendBarcode("QC005210111GB");
			clk.clickBackButton();
			clk.clickOK();
			//selecting process
			if(j != (sbprcslst-1))
			{
				processLayer.click();
				IndoorProcessScreen mthd= new IndoorProcessScreen(driver);
				mthd.selectprocess("DO Inward Sorting");
			}			

		}
	}
}
