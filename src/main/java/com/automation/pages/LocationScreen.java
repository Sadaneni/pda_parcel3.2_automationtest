package com.automation.pages;


import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

import java.util.List;
import java.util.concurrent.TimeUnit;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.openqa.selenium.support.PageFactory;

public class LocationScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvLocation")
	private MobileElement tvlocaton;

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private List<MobileElement> selectlocation;
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvLocation")
	private MobileElement cont;
	
	@AndroidFindBy(id="com.royalmail.pda:id/btnSubmit")
	private MobileElement button;
	
	@AndroidFindBy(id="com.royalmail.pda:id/ivLocation")
	private MobileElement locationIcon;

	public LocationScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void clickTvLocation(){
		 tvlocaton.click();
	}
	public void clickLocation(int index) {
		selectlocation.get(index).click();
	}
	public HomePageScreen clickContinueButton() throws InterruptedException{
		button.click();
		Thread.sleep(10000);
		return new HomePageScreen(driver);
	}
	
	public Boolean assert_locationIcon(){
		Boolean islocationIcon= isElementVisible(locationIcon,driver);
		return islocationIcon;
	}
	
	public void SelectLocation(String arg1)
    {
        //System.out.println("value of arg1 is :" + arg1);
        List<MobileElement> loc = selectlocation;
        int loclst = loc.size();
        System.out.println("The size for location is: " + loclst);
        for (int j = 0; j < loclst; j++) 
        {
            String locnme = loc.get(j).getText();
            System.out.println(locnme);
            if(locnme.equals(arg1))
            {
                loc.get(j).click();
                break;
            }
        }
    }
	public HomePageScreen selectLocationByText(String locationName){
       this.clickTvLocation();
       for (MobileElement loc :
               selectlocation) {
           if (loc.getText().contains(locationName)) {
               loc.click();
               button.click();
               break;
           }
       }
       return new HomePageScreen(driver);
   }
	
	public void selectAllLocations(){
		tvlocaton.click();
		List<MobileElement> loc = selectlocation;
		int loclst = loc.size();
		for (int j = 0; j < loclst; j++) 
		{
			String locnme = loc.get(j).getText();
			loc.get(j).click();
			tvlocaton.click();	
		}
	}
}

