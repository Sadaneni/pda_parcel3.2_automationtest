package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class DescriptionOtherSafePlaceScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/safe_othertv")
	private MobileElement describeText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/ed_addl_info")
	private MobileElement desTextBox;
	
	@AndroidFindBy(id="com.royalmail.pda:id/cameraButton")
	private MobileElement takePhoto;
	
	public DescriptionOtherSafePlaceScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public Boolean assert_describeText(){
		Boolean ischangeStatusText= isElementVisible(describeText,driver);
		return ischangeStatusText;
	}
	
	public void enterDescription(String Description) throws InterruptedException{
		desTextBox.sendKeys( Description);
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
		}
	}
	
	public void tapOnTakePhoto() throws InterruptedException{
		takePhoto.click();
		Thread.sleep(3000);
	}
	
}
