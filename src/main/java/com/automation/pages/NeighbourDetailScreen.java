package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class NeighbourDetailScreen extends Page {


	@AndroidFindBy(className="android.widget.EditText")
	private List<MobileElement>customer;
    
	@AndroidFindBy(id="com.royalmail.pda:id/name")
	private MobileElement neighbourName;
	
	@AndroidFindBy(id="com.royalmail.pda:id/address")
	private MobileElement neighbourAddress;
	
	@AndroidFindBy(id="com.royalmail.pda:id/txtvw_street_name")
	private MobileElement streetName;
	
	@AndroidFindBy(id="com.royalmail.pda:id/proceed_button")
	private MobileElement done;
	
	@AndroidFindBy(className="android.widget.EditText")
	private MobileElement customerName;

	public NeighbourDetailScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void enterNeighbourName(int index){
		customer.get(index).sendKeys("RMG");
	}
	
	public void enterNeighbourAddress(int index){
		customer.get(index).sendKeys("185");
	}
	
	public void tapDone() throws InterruptedException{
		done.click();
		Thread.sleep(5000);
	}
	public void enterStreetName(int index){
		customer.get(index).sendKeys("Farringdon");
		driver.hideKeyboard();
	}
	public Boolean assert_neighbourName(){
		Boolean isneighbourName= isElementVisible(neighbourName,driver);
		return isneighbourName;
	} 
   
	public Boolean assert_neighbourAddress(){
		Boolean isneighbourAddress= isElementVisible(neighbourAddress,driver);
		return isneighbourAddress;
	} 
	
	public Boolean assert_streetName(){
		Boolean isstreetName= isElementVisible(streetName,driver);
		return isstreetName;
	} 
	
	public void enterCustomerName(String Name){
		customerName.sendKeys(Name);
		driver.hideKeyboard();
	}
	
}
