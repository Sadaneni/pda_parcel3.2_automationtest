package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class AknowledgementScreen extends Page{
	
	@AndroidFindBy(id="com.royalmail.pda:id/name_address_text")
	private MobileElement nameText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/iconView")
	private MobileElement royalMailIcon;
	
	@AndroidFindBy(id="com.royalmail.pda:id/signature_field")
	private MobileElement signature;
	
	@AndroidFindBy(id="com.royalmail.pda:id/done_button")
	private MobileElement done;
	
	public AknowledgementScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	

	public void screenRotate(){
		driver.rotate(ScreenOrientation.PORTRAIT);
	}
	public Boolean assert_nameText(){
		Boolean isnameText= isElementVisible(nameText,driver);
		return isnameText;
	}

	public Boolean assert_royalMailIcon(){
		Boolean isroyalMailIcon= isElementVisible(royalMailIcon,driver);
		return isroyalMailIcon;
	}

	
	public void signSignature( String Sign){
		signature.sendKeys(Sign);
	}
	
	public void clickDone() throws InterruptedException{
		done.click();
		Thread.sleep(2000);
	}
	
	public void sign(){
		Dimension dimens = driver.manage().window().getSize();
		TouchAction touch = new TouchAction(driver);
		touch.press(360, 540).waitAction(2000).moveTo(600,800).release().perform();
	}
	public void sign1(){
		Dimension dimens = driver.manage().window().getSize();
		TouchAction touch = new TouchAction(driver);
		touch.press(360, 540).waitAction(2000).moveTo(190,800).release().perform();	
	}
	public void sign2(){
		Dimension dimens = driver.manage().window().getSize();
		TouchAction touch = new TouchAction(driver);
		touch.press(215,670).waitAction(2000).moveTo(530,680).release().perform();	
	}
}
	
	  
	
