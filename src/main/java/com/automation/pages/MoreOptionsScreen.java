package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class MoreOptionsScreen extends Page {

	@AndroidFindBy(className = "android.widget.LinearLayout")
	private List<MobileElement> optionlist;

	@AndroidFindBy(id = "com.royalmail.pda:id/dialog_btn2")
	private MobileElement cancel;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_title")
	private MobileElement AppLock;

	public MoreOptionsScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver,
				new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), this);
	}

	public void clickCancel() {
		cancel.click();
	}

	public void selectAllOptions() {
		List<MobileElement> opt = optionlist;
		int Optlst = opt.size();
		for (int j = 0; j < Optlst; j++) {
				String locnme = opt.get(j).getText();
				opt.get(j).click();
	    		try{
					clickCancel();
				}catch(Exception e){}
				driver.navigate().back();
				HomePageScreen hom = new HomePageScreen(driver);
				hom.clickMoreOption();
			}
	}
}
