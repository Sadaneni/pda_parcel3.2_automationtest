package com.automation.pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class HomePageScreen extends Page {
	 Dimension size;
	@AndroidFindBy(className="android.widget.Button")
	private  List<MobileElement> home;
	
	@AndroidFindBy(id="android:id/action_bar_title")
	private MobileElement homeText;
	
	@AndroidFindBy(className="android.widget.Button")
    private MobileElement homemenu;
    
	@AndroidFindBy(className="android.widget.ImageButton")
    private MobileElement moreoptions;
	
    @AndroidFindBy(id="com.royalmail.pda:id/memo_list")
    private MobileElement homescreenmemo;
    
    @AndroidFindBy(id="com.royalmail.pda:id/writeMemoBtn")
    private MobileElement memobtn;
    
    @AndroidFindBy(id="com.royalmail.pda:id/memo_edit")
    private MobileElement typememo;
    
    @AndroidFindBy(id="com.royalmail.pda:id/memo_send")
    private MobileElement sendmemo;    
    
    @AndroidFindBy(className="android.widget.RelativeLayout")
    private MobileElement verifymemo;

    @AndroidFindBy(className="android.webkit.WebView")
    private MobileElement verifyWatchandWin;
    
    @AndroidFindBy(id="android:id/action_bar")
    private MobileElement verifyHomeScreenNavigated;
    
    @AndroidFindBy(id="android:id/title")
    private List<MobileElement> settings;
	
	public HomePageScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public Boolean assert_homeText(){
		Boolean ishomeText= isElementVisible(homeText,driver);
		return ishomeText;
	}

	public Boolean assert_homescreenmemo()
    {
        Boolean ishomescreenmemo= isElementVisible(homescreenmemo,driver);
        return ishomescreenmemo;
    }
    
    public Boolean assert_Verifymemo()
    {
        Boolean isverifymemo= isElementVisible(verifymemo,driver);
        return isverifymemo;
    }
    
    public Boolean assert_VerifyHomeScreenNavigated()
    {
        Boolean isverifyWandW= isElementVisible(verifyHomeScreenNavigated,driver);
        return isverifyWandW;
    }
	public void clickOutDoor(int index){
		home.get(index).click();
	}
	
	public void clickMoreOption(){
		moreoptions.click();
	}
	
	public void clickWriteamemobutton()
    {
        memobtn.click();
    }
	public void clickIndoor(int index){
		home.get(index).click();
	}
	
	public void doSomething(String val) throws InterruptedException{
		for (MobileElement vd:settings){
			if(vd.getText().equals(val)){
				vd.click();
				break;
			}
		}
	}
	public void SelectHomeScreenOption1(String arg1)
    {
        List<MobileElement> homelist = home;
        int listcnt = homelist.size();
        for (int j = 0; j < listcnt; j++) 
        {
            String  listname = homelist.get(j).getText();
            if(listname.equals(arg1))
            {
                homelist.get(j).click();
                break;
            }
        }        
    }
	
	public void continueToManageOutDoor() throws InterruptedException{
		Thread.sleep(0000);
	}
	
	public void continueToIndoor() throws InterruptedException{
		Thread.sleep(0000);
	}
	
	 public void ClickAllOptions() throws InterruptedException, IOException
	    {
	        List<MobileElement> homelist = home;
	        int listcnt = homelist.size();
	        //System.out.println("The no.of options present on Home page are:" + sbprcslst);
	        
	        for (int j = 0; j < listcnt; j++) 
	        {
	            String  listname = homelist.get(j).getText();
	            //System.out.println(listname);
	            homelist.get(j).click();
	                        
	            //Capture Screenshot
	            GenericMethods gm = new GenericMethods(driver);
	            gm.CaptureScreenshot(listname);            
	                        
	            if((listname.equals("LOGOUT")))
	            {
	                GenericMethods genmthd = new GenericMethods(driver);
	                genmthd.Logout_ClickNo();
	            }
	            else if((listname.equals("INDOOR")))
	            {
	                driver.navigate().back();
	                EnquiryOfficeScreen clk = new EnquiryOfficeScreen(driver);
	                clk.clickBackButton();
	            }
	            else
	            {
	                if((listname.equals("WATCH&WIN")) || (listname.equals("NAVIGATION")))
	                    driver.navigate().back();
	                else
	                {
	                    EnquiryOfficeScreen clk = new EnquiryOfficeScreen(driver);
	                    clk.clickBackButton();
	                }
	            }            
	        }        
	    }
	 
	 public void signature()
	    {
	        size = driver.manage().window().getSize();
	          System.out.println(size);
	          driver.swipe(503, 640, 7, 640, 3000);
	          
	    }
	            
	    public String getHomeScreenOptions()
	    {
	        String optns = null; 
	        
	        List<MobileElement> homelist = home;
	        int listcnt = homelist.size();
	                
	        for (int j = 0; j < listcnt; j++) 
	        {
	            String  listname = homelist.get(j).getText();
	            optns = optns + "," +listname;
	        }            
	        optns = optns.replace("null,", "");
	        return optns;
	    }

	    public void SendMemo(String arg1)
	    {
	        typememo.sendKeys(arg1);
	        sendmemo.click();
	        
	    }
	    
	    
	    public void VerifyMemoMessage(String arg1)
	    {
	        verifymemo.click();
	    }
}
