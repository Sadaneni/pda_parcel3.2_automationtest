package com.automation.pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class EnquiryOfficeScreen extends Page
{

	public EnquiryOfficeScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	@AndroidFindBy(id="android:id/home")
	private MobileElement backbutton;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement ok;
	
	public void clickBackButton(){
		backbutton.click();
	}
	
	public void clickOK(){
		ok.click();
	}
	public void sendBarcode(String Barcode) {
		//	String barcodeQueryString = String.format("adb shell am broadcast -a com.royalmail.pda.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA \"%s\"", Barcode);
			String barcodeQueryString = String.format("adb shell am broadcast -a com.royalmail.pda.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA " + Barcode + " --es " + "TEST_BARCODE_TYPE_DATA " + "1D");
			AdbBarcodeClient barcode = new AdbBarcodeClient();
			try {
				barcode.runADBCommand(barcodeQueryString);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		}
		
}
