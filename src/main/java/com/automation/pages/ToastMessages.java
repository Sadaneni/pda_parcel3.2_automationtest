package com.automation.pages;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;

public class ToastMessages extends Page {

	public ToastMessages(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void VerifyToastMessage(){
	//	String Text =OCR(image(driver));
		String Text1=Verify(driver);
		System.out.println(Text1);
		Assert.assertTrue(Text1.contains("Username or PIN Is Incorrect."), "Username or PIN Is Incorrect.");
	}
	
	@SuppressWarnings("rawtypes")
	public static String image(AppiumDriver<MobileElement>driver){
		File targetFile = null;
		try{
			File scrFile =driver.getScreenshotAs(OutputType.FILE);
			String fileName =UUID.randomUUID().toString();
			targetFile = new File("./screenshots/"+fileName +".png");
			FileUtils.copyFile(scrFile, targetFile);
			System.out.println(targetFile.toString());
		}catch(IOException e){
			e.printStackTrace();
		}
		return targetFile.toString();
	}
	
	public static String OCR(String ImagePath){
		String result = null;
		File imageFile = new File(ImagePath);
	ITesseract instance = new Tesseract();
	File tessDataFolder = LoadLibs.extractTessResources("tessdata");
	instance.setDatapath(tessDataFolder.getAbsolutePath());
		try{
			result = instance.doOCR(imageFile);
		}
		catch(TesseractException e){
			System.out.println(e.getMessage());
		}
		return result;
	}
	
	public String Verify(AppiumDriver driver){
		String result = null;
		File scrFile =driver.getScreenshotAs(OutputType.FILE);
		ITesseract instance = new Tesseract();
		File tessDataFolder = LoadLibs.extractTessResources("tessdata");
		instance.setDatapath(tessDataFolder.getAbsolutePath());
		try{
			result = instance.doOCR(scrFile);
		}catch (TesseractException e){
			System.err.println(e.getMessage());
		}
		return result;
	}
}
