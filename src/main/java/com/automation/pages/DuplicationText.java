package com.automation.pages;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class DuplicationText extends Page{

	public DuplicationText(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void duplicateText(){
	List<MobileElement> elements = driver.findElementsById("com.royalmail.pda:id/statusItem");
	Map<String, Integer> textCounts = new HashMap<>();

	for (MobileElement element : elements) {

	    String text = element.getText();
	        if (textCounts.containsKey(text)) {

	            textCounts.put(text, textCounts.get(text) + 1);
	        } else {

	        textCounts.put(text, 1);
	    }
	}
	for (Map.Entry<String, Integer> entry : textCounts.entrySet()) {
	    System.out.println(entry.getKey()+" : "+entry.getValue());
	}

   }
	
}
