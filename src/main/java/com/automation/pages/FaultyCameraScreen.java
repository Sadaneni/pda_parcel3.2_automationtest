package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class FaultyCameraScreen extends Page{
	
	@AndroidFindBy(id="com.royalmail.pda:id/checkBox")
	private MobileElement checkBox;
	
	@AndroidFindBy(id="com.royalmail.pda:id/camera_icon")
	private MobileElement cameraIcon;

	@AndroidFindBy(id="com.royalmail.pda:id/submit")
	private MobileElement confirm;
	
	public FaultyCameraScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	public Boolean assert_cameraIcon(){
		Boolean iscameraIcon= isElementVisible(cameraIcon,driver);
		return iscameraIcon;
	}
	
	public void clickCheckBox(){
		checkBox.click();
	}
	
	public void tapOnConfirmAndSubmit() throws InterruptedException{
		confirm.click();
		Thread.sleep(3000);
	}
}
