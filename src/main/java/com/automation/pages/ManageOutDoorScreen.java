package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class ManageOutDoorScreen extends Page{

	@AndroidFindBy(id="com.royalmail.pda:id/btn_continue")
	private MobileElement contin;

	@AndroidFindBy(id="com.royalmail.pda:id/deliveryRouteEditText")
	private MobileElement deliveryRoute;

	@AndroidFindBy(id="android:id/action_bar_title")
	private MobileElement outDoorText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/collectionRouteEditText")
	private MobileElement CollectionRoute;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn3")
	private MobileElement RetryButton;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement ContinueButton;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn2")
	private MobileElement CancelButton;
	
	@AndroidFindBy(id="com.royalmail.pda:id/btManageManifest")
	private MobileElement MangeOutdoorButton;

	public ManageOutDoorScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	

	public Boolean assert_outDoorText(){
		Boolean isoutDoorText= isElementVisible(outDoorText,driver);
		return isoutDoorText;
	}

	public  PendingScreen clickManageOutDoor(){
		contin.click();
		return new PendingScreen(driver);
	}

	public void EnterDeliveryRoute(String Number) throws InterruptedException{
		deliveryRoute.sendKeys(Number);
		try {
			driver.hideKeyboard();
		} catch (Exception e) {
		}
		Thread.sleep(2000);
	}
	
	
	public void EnterCollectionRoute(String arg1) throws InterruptedException{
		CollectionRoute.sendKeys(arg1);
		try {
			driver.hideKeyboard();			
		} catch (Exception e) {
		}
		Thread.sleep(2000);
		
	}

	public void clickDownloadRoute() throws InterruptedException{
		contin.click();
		Thread.sleep(2000);
	}
	
	public void clickRetryButton() throws InterruptedException
	{
		Thread.sleep(7000);
		boolean Val = isElementVisible(MangeOutdoorButton,driver);
			if(!Val)
			{	
			for(int i=0;i<2;i++)
			{		

				try {
						RetryButton.click();	
						Thread.sleep(2000);
					} 
				catch (Exception e) {}
			}
		}
	}
	
	public void clickContinueButton() throws InterruptedException
    {
        Thread.sleep(7000);
        boolean Val = isElementVisible(MangeOutdoorButton,driver);
        if(!Val)
        {    
            try 
            {
                ContinueButton.click();
                Thread.sleep(1000);
            } 
            catch (Exception e) {}
        }
        
    }
}


