package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class ChooseSafePlaceScreen extends Page{
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private List<MobileElement> safeList;

	@AndroidFindBy(id="com.royalmail.pda:id/proceed_button")
	private MobileElement cont;
	
	public ChooseSafePlaceScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void selectSafeList(int index){
		safeList.get(index).click();
	}
	
	public void doSomething(String val) throws InterruptedException{
		for (MobileElement vd:safeList){
			if(vd.getText().equals(val)){
				vd.click();
				break;
			}
		}
	}

	public void tapOnContinue(){
		cont.click();
	}
}
