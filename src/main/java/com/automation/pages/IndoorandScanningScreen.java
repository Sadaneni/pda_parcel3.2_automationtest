package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class IndoorandScanningScreen extends Page {

	public IndoorandScanningScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	@AndroidFindBy(id="com.royalmail.pda:id/customer_spinner")
	   private MobileElement customer;

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerDroped")
	private List<MobileElement> dropdown;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/upicode")
	private MobileElement scanitem;	
	
	
	 public void clickCustomer()
	   {
		   customer.click();
	   }

	  public void selectCustomer(String arg1)
	   {
	   	
		   List<MobileElement> cus = dropdown;
		   int cusSize= cus.size();
	   	   for( int i=0; i<cusSize; i++)
	   	   {
	   		  String cusname = cus.get(i).getText();
	   		  if(cusname.equals(arg1))
	          {
	   			cus.get(i).click();
	   			break;
	          }
	   	   }
	   }
	  
	  public boolean assert_Scanneditems(){
			Boolean isscanneditem = isElementVisible(scanitem,driver);
			return isscanneditem;
		}
	
}
