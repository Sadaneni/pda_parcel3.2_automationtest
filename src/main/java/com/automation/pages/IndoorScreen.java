package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class IndoorScreen extends Page{
	
	@AndroidFindBy(id="android:id/home")
	private MobileElement back;
	
	@AndroidFindBy(id="android:id/action_bar_title")
	private MobileElement indexText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/indoor_container")
	private MobileElement contin;
	
	public IndoorScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	
	public void clickBackArrow(){
		back.click();
	}
	public Boolean assert_indexText(){
		Boolean isindexText= isElementVisible(indexText,driver);
		return isindexText;
	}

	public void tapOnContinue(){
		contin.click();
	}
	
}
