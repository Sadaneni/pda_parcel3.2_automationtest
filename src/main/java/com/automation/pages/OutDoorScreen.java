package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class OutDoorScreen extends Page {

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerDroped")
	private List<MobileElement> adhoc;

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private MobileElement Outprcs;	

	@AndroidFindBy(id="com.royalmail.pda:id/incident_value")
	private MobileElement NoDrpdwn;

	@AndroidFindBy(id="com.royalmail.pda:id/groupHeader")
	private List<MobileElement> RsnDrpdwn;	

	@AndroidFindBy(id="android:id/action_bar_title")
	private MobileElement outDoorText;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement Yesbtn;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn2")
	private MobileElement Nobtn;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn3")
	private MobileElement Cancelbtn;

	@AndroidFindBy(id="com.royalmail.pda:id/proceed_button")
	private MobileElement No_Sbtbtn;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_message")
	private MobileElement msg_outdoor;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_name")
	private MobileElement AdhocCollection_name;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_address1")
	private MobileElement AdhocCollection_address1;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_address2")
	private MobileElement AdhocCollection_address2;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_city")
	private MobileElement AdhocCollection_city;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_pin_number")
	private MobileElement AdhocCollection_postcode;
	
	@AndroidFindBy(id="com.royalmail.pda:id/editText_phone")
	private MobileElement AdhocCollection_phno;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/btn_continue")
	private MobileElement Ctn_btn;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/done_button")
	private MobileElement sbt_btn;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/stampCheck")
	private MobileElement stmp_chk;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/submitBtn")
	private MobileElement dne_btn;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/et_sealcode")
	private MobileElement Route_AdhocCollection;	
	
	@AndroidFindBy(id="com.royalmail.pda:id/btRight")
	private MobileElement OK_btn_adhoccollection;	

	public OutDoorScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	public AdhocDeliveryScreen clickAdhocDelivery(int index){
		adhoc.get(index).click();
		return new AdhocDeliveryScreen(driver);
	}

	public void slctprcsDropdown()
	{
		Outprcs.click();
	}

	public void clickSbmtbtn() throws InterruptedException
	{
		Thread.sleep(1000);
		No_Sbtbtn.click();
	}

	public void slctNobtnDropdown()
	{
		NoDrpdwn.click();
	}

	public void clickCancel() throws InterruptedException
	{
		Cancelbtn.click();
		Thread.sleep(1000);
	}

	public void clickYes() throws InterruptedException
	{
		Yesbtn.click();
		Thread.sleep(1000);
	}

	public void clickNo() throws InterruptedException
	{
		Nobtn.click();
		Thread.sleep(1000);
	}

	public void continueAdhocScreen() throws InterruptedException{
		Thread.sleep(0000);
	}
	
	public Boolean assert_outDoorText(){
		Boolean isoutDoorText= isElementVisible(outDoorText,driver);
		return isoutDoorText;
	}

	public AdhocDeliveryScreen selectOutdoorprocess(String arg1)
	{
		int indx=1;
		List<MobileElement> prcs = adhoc;
		int prcslst = prcs.size();
		for (int s = 0; s < prcslst; s++)
		{
			String prcsnm = prcs.get(s).getText();
			if(prcsnm.equals(arg1))
			{		
				indx = s;
				break;
			}
		}
		prcs.get(indx).click();
		return new AdhocDeliveryScreen(driver);
	}

	public void selectReasonValue(String arg1)
	{
		int indx=1;
		List<MobileElement> prcs = RsnDrpdwn;
		int prcslst = prcs.size();
		for (int s = 0; s < prcslst; s++)
		{
			String prcsnm = prcs.get(s).getText();
			if(prcsnm.equals(arg1))
			{		
				indx = s;
				break;
			}
		}
		prcs.get(indx).click();

	}
	
	public void assert_msg(String Expectedmsg)
	{
		String msg = msg_outdoor.getText();
		//assert.assertEquals(msg, Expectedmsg);
		Assert.assertEquals(Expectedmsg, msg);		
	}	
	
	public void Enter_adhoccollection_details() throws InterruptedException
	{
		AdhocCollection_name.sendKeys("185");
		Thread.sleep(1000);
		AdhocCollection_address1.sendKeys("FARRINGDON");
		Thread.sleep(1000);
		AdhocCollection_address2.sendKeys("LONDON");
		Thread.sleep(1000);
		driver.hideKeyboard();
	}
	
	public void clk_ctn() throws InterruptedException
	{
		Ctn_btn.click();
		Thread.sleep(1000);
	}
	
	public void clk_sbt() throws InterruptedException
	{
		sbt_btn.click();
		Thread.sleep(1000);		
	}
	
	public void select_mailtype() throws InterruptedException
	{
		stmp_chk.click();
		Thread.sleep(1000);		
	}
	
	public void clk_dne() throws InterruptedException
	{
		dne_btn.click();
		Thread.sleep(1000);
	}
	
	public void Route_AdhocCollection(String arg1)
	{
		Route_AdhocCollection.sendKeys(arg1);
	}
	
	public void clk_OK_AdhocCollection()
	{
		OK_btn_adhoccollection.click();
	}

}
