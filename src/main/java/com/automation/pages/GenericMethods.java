package com.automation.pages;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class GenericMethods extends Page
{
	Dimension size;

	public GenericMethods(AppiumDriver<MobileElement> driver) {
		super(driver);
		// TODO Auto-generated constructor stub
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_message")
	private MobileElement ErrMsg;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement btn;
	
	@AndroidFindBy(id="android:id/button2")
    private MobileElement Logout_no;
    
    @AndroidFindBy(id="android:id/button1")
    private MobileElement Logout_yes;
    
    @AndroidFindBy(id="com.royalmail.pda:id/done_button")
    private MobileElement ClickDonebtn;
	
	
	public void CaptureScreenshot(String name) throws IOException
	{
		File srcfile = driver.getScreenshotAs(OutputType.FILE);
		String trt = "C:\\Users\\Ramana.Sadaneni\\Desktop\\screenshots\\";
		File trgtfile = new File(trt + name +".png");
		FileUtils.copyFile(srcfile,trgtfile);
	}
	
	public String CaptureErrMsg()
	{
		String ErMsg = ErrMsg.getText();
		return ErMsg;
	}
	
	public void clickbtn()
	{
		btn.click();
	}
	
	public void clickOK(){
		btn.click();
	}
	
	public void wifi_0ff() throws IOException{
//       String cmd ="adb -s " +"15279522502126"+" shell am start -n io.appium.settings/.Settings -e wifi off";
//	   Process exec = Runtime.getRuntime().exec(cmd);
	}
	
	public void Logout_ClickYes()
    {
        Logout_yes.click();        
    }
    
    public void Logout_ClickNo()
    {
        Logout_no.click();        
    }
    
    public void ClickDone()
    {
        ClickDonebtn.click();
    }
    
    public void click_BackButton() throws InterruptedException
    {
        Thread.sleep(1000);
        driver.navigate().back();
    }
    
    public void swipeLeft(Integer duration) {
		Dimension dimens = driver.manage().window().getSize();
		int y = (int) (dimens.getHeight() * 0.9);
		int startX = (int) (dimens.getWidth() * 0.90);
		int endX = (int) (dimens.getWidth() * 0.400);
		driver.swipe(startX, y, endX, y, duration);
	}
 
    public void SwipeScreenRight() throws InterruptedException
    {
        size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.70);
        int endx = (int) (size.width * 0.01);
        int starty = size.height / 2;
        System.out.println("startx = " + startx + " ,endx = " + endx + " , starty = " + starty);
        //Swipe from Right to Left.
        driver.swipe(startx, starty, endx, starty, 1000);        
    }

    public void SwipeScreenLeft() throws InterruptedException
    {
        size = driver.manage().window().getSize();
        int startx = (int) (size.width * 0.70);
        int endx = (int) (size.width * 0.01);
        int starty = size.height / 2;
        //Swipe from Left to Right
        driver.swipe(endx, starty, startx, starty, 1000);            
    }
	
}
