package com.automation.pages;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class IndoorSubProcessScreen extends Page{

	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerDroped")
	private List<MobileElement> subProcess;

	@AndroidFindBy(id="com.royalmail.pda:id/indoor_container")
	private MobileElement contin;
	
	@AndroidFindBy(id="com.royalmail.pda:id/tvSpinnerSelected")
	private MobileElement processLayer;

	public IndoorSubProcessScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	public void selectSubProcess(int index){
		subProcess.get(index).click();
	}

	public IndoorScreen selectSubProcessByText(String SubProcess){
		for (MobileElement Pro :
			subProcess) {
			if (Pro.getText().contains(SubProcess)) {
				Pro.click();
				break;
			}
		}
		return new IndoorScreen(driver);
	}

	public void tapOnContinue(){
		contin.click();
	}
	
	public String CaptureSubProcess()
    {
         String sbprcsoptns = null;
         List<MobileElement> sbprcs = subProcess;
         int sbprcslst = sbprcs.size();
         for (int s = 0; s < sbprcslst; s++)
         {
             String sbprcsnm = sbprcs.get(s).getText();
             sbprcsoptns = sbprcsoptns + "," + sbprcsnm;        
         }            
         //System.out.println("The values under sub process are:" + sbprcsoptns);
         sbprcsoptns = sbprcsoptns.replace("null,", "");
         
         return sbprcsoptns;
     }
	
	public void selectSubprocess() throws InterruptedException, IOException
	{
		List<MobileElement> sbprcs = subProcess;
		int sbprcslst = sbprcs.size();
		for (int j = 0; j < sbprcslst; j++) 
		{
			String sbprsnme = sbprcs.get(j).getText();
			sbprcs.get(j).click();
			GenericMethods gm = new GenericMethods(driver);
			gm.CaptureScreenshot(sbprsnme);			
			EnquiryOfficeScreen clk = new EnquiryOfficeScreen(driver);
			clk.sendBarcode("QC005210111GB");
			clk.clickBackButton();
			clk.clickOK();

			//selecting process
			if(j != (sbprcslst-1))
			{
				processLayer.click();
				IndoorProcessScreen mthd= new IndoorProcessScreen(driver);
				mthd.selectprocess("Enquiry Office");
			}			

		}
	}
}