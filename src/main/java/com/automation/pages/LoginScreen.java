package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class LoginScreen extends Page  {
	
	@AndroidFindBy(id="login_edit_username")
	private  MobileElement login;
	
	
	
	@AndroidFindBy(id="login_edit_password")
	private MobileElement password;
	
	@AndroidFindBy(id="login_btn_login")
	private MobileElement loginButton;
	
	public LoginScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	
/*	public LoginScreen() {
		waitOnVisibilityOfElement(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, driver.findElement(By.id("com.royalmail.pda:id/login_edit_username")));
		 PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)),this);
	}
	*/
	 public void EnterUserName(String EnterUser){
		 login.sendKeys(EnterUser);
	 }
	 
	 public void SelectEnterUserName_element(){
         login.click();
         login.clear();
     }
	 
	 public void EnterPin(String EnterPin){
		 password.click();
		 password.sendKeys(EnterPin);
		
	 }
	 
	 public Boolean assert_Loginscreen(){
         Boolean isloginButton= isElementVisible(loginButton,driver);
         return isloginButton;
     }
	 
	 public LocationScreen clickLoginButton(){
		 loginButton.click();
		 try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 return new LocationScreen(driver);
	 } 
}
