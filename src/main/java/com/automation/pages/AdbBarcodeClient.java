package com.automation.pages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class AdbBarcodeClient {
	public String runADBCommand(String adbCommand) throws IOException {
        System.out.println("Running given command= " + adbCommand + "$$$");
        StringBuffer returnValue = new StringBuffer();
        String line;
        InputStream inStream = null;
        try {
            System.out.println("adbCommand = " + adbCommand);
            Process process = Runtime.getRuntime().exec(adbCommand);

            // process.waitFor();/
            inStream = process.getInputStream();
            BufferedReader brCleanUp = new BufferedReader(
                    new InputStreamReader(inStream));
            while ((line = brCleanUp.readLine()) != null) {
                if (!line.equals("")) {
                    System.out.println("After exec");
                    System.out.println("Line=" + line);

                }

                // returnValue = returnValue + line + "\n";
                returnValue.append(line).append("\n");
            }

            brCleanUp.close();
            try {


                process.waitFor();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error: " + e.getMessage());
        }
        System.out.println(returnValue.toString() + "@@");
        return returnValue.toString();
    }
}

