package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class CameraScreen extends Page{
	
//	@AndroidFindBy(id="com.android.camera2:id/shutter_button")
//  	private MobileElement takePhoto;
	
	@AndroidFindBy(className ="android.widget.Button")
	private MobileElement takePhoto;
	
	@AndroidFindBy(id="com.android.camera2:id/btn_cancel")
	private MobileElement cancelPhoto;
	
	@AndroidFindBy(id="com.android.camera2:id/menu_timer_indicator")
	private MobileElement menu;
	
	public CameraScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
   public void tapOnMenu(){
	   menu.click();
   }
   
	public void takePicture() throws InterruptedException{
		takePhoto.click();
		Thread.sleep(3000);
	}
	
	public void cancelPicture(){
		cancelPhoto.click();
	}
}
