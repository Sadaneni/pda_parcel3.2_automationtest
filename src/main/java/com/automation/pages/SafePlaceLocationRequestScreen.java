package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class SafePlaceLocationRequestScreen extends Page {
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_title")
	private MobileElement safePlaceText;
	
	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement camera;
	
	public SafePlaceLocationRequestScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public Boolean assert_safePlaceText(){
		Boolean issafePlaceText= isElementVisible(safePlaceText,driver);
		return issafePlaceText;
	}
	
	public void tapOnCameraIcon(){
		camera.click();
	}
}
