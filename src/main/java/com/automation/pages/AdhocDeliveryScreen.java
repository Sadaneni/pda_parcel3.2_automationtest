package com.automation.pages;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;


public class AdhocDeliveryScreen extends Page{

	@AndroidFindBy(id="com.royalmail.pda:id/deliverymode1")
	private MobileElement customer;

	@AndroidFindBy(id="com.royalmail.pda:id/deliverymode2")
	private MobileElement neighbour;

	@AndroidFindBy(id="com.royalmail.pda:id/deliverymode3")
	private MobileElement safePlace;

	@AndroidFindBy(id="com.royalmail.pda:id/disclosure")
	private MobileElement arrow;

	@AndroidFindBy(id="com.royalmail.pda:id/submit")
	private MobileElement submit;

	@AndroidFindBy(id="com.royalmail.pda:id/apply_all")
	private MobileElement changeStatusText;

	@AndroidFindBy(id="com.royalmail.pda:id/centerLayout")
	private List<MobileElement> barcodeList;

	@AndroidFindBy(id="com.royalmail.pda:id/et_upi_manually")
	private MobileElement enternumber;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn1")
	private MobileElement ok;

	@AndroidFindBy(id="com.royalmail.pda:id/dialog_btn2")
	private MobileElement cancel;
	
	 @AndroidFindBy(id="com.royalmail.pda:id/done_button")
	   private MobileElement clkSubmit;

	public AdhocDeliveryScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	public void selectOK(){
		ok.click();
	}

	public void selectCancel(){
		cancel.click();
	}
	public void clickNumber(){
		enternumber.click();
	}

	public void enterBarcode(String Number){
		enternumber.sendKeys(Number);
	}

	public void selectBarcode(int index){
		barcodeList.get(index).click();
	}
	public ChangeStatusScreen clickArrow(){
		arrow.click();
		return new ChangeStatusScreen(driver);
	}

	public void clickSubmitadhoc(){
		submit.click();
	}

	public void clickSubmit_jobdetails(){
		clkSubmit.click();
	}
	
	public Boolean assert_customer(){
		Boolean iscustomer= isElementVisible(customer,driver);
		return iscustomer;
	}

	public Boolean assert_neighbour(){
		Boolean isneighbour= isElementVisible(neighbour,driver);
		return isneighbour;
	}

	public Boolean assert_safePlace(){
		Boolean issafePlace= isElementVisible(safePlace,driver);
		return issafePlace;
	}

	public Boolean assert_changeStatusText(){
		Boolean ischangeStatusText= isElementVisible(changeStatusText,driver);
		return ischangeStatusText;
	}

	public void sendBarcode(String Barcode) {
	//	String barcodeQueryString = String.format("adb shell am broadcast -a com.royalmail.pda.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA \"%s\"", Barcode);
			String barcodeQueryString = String.format("adb shell am broadcast -a com.royalmail.pda.TEST_BARCODE_INTENT --es TEST_BARCODE_DATA " + Barcode + " --es " + "TEST_BARCODE_TYPE_DATA " + "1D");
		AdbBarcodeClient barcode = new AdbBarcodeClient();
		try {
			barcode.runADBCommand(barcodeQueryString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}


	public AdhocDeliveryScreen clicksearchbutton(){
		AndroidDriver<?> androidDriver = (AndroidDriver<?>)driver;
		androidDriver.pressKeyCode(84); // not working
		// Using TouchAction (not good solution but seems to be the official solution provided in appium discussion!)
		Dimension dimens = driver.manage().window().getSize();
		// try to determine position of magnifier icon as % of screen size
		int x = (int) ((dimens.getWidth()  * 1000)/1080); 
		int y = (int) ((dimens.getHeight() * 1698)/1794);
		TouchAction action = new TouchAction(driver);
		action.tap(x,y).perform();
		return new AdhocDeliveryScreen(driver);
	}

}

