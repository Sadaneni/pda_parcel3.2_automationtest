package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class SubmitPhotoScreen extends Page{
	
	@AndroidFindBy(id="com.royalmail.pda:id/retakeBtn")
	private MobileElement retake;
	
	@AndroidFindBy(id="com.royalmail.pda:id/submit")
	private MobileElement submitPhoto;
	
	@AndroidFindBy(id="com.royalmail.pda:id/textView3")
	private MobileElement preview;
	
	@AndroidFindBy(id="com.royalmail.pda:id/faultTV")
	private MobileElement faultyCamera;
	
	public SubmitPhotoScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	

	public Boolean assert_preview(){
		Boolean ispreview= isElementVisible(preview,driver);
		return ispreview;
	}

	public void tapSubmitPhoto() throws InterruptedException{
		submitPhoto.click();
		Thread.sleep(5000);
	}
	
	public void tapRetake(){
		retake.click();
	}
	
	public void tapOnFaultyCamera(){
		faultyCamera.click();
	}
}
