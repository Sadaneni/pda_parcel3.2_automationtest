package com.automation.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class SettingScreen extends Page{
	
	
	@AndroidFindBy(id="android:id/home")
	private MobileElement back;
	
	@AndroidFindBy(id="com.royalmail.pda:id/btnBluetooth")
	private MobileElement blueToothUtility;

	@AndroidFindBy(id="com.royalmail.pda:id/btnAccessibility")
	private MobileElement blueToothAccess;
	
	@AndroidFindBy(id="com.royalmail.pda:id/txtDeviceId")
	private MobileElement deviceId;
	
	@AndroidFindBy(id="com.royalmail.pda:id/txtVersion")
	private MobileElement deviceVers;
	
	@AndroidFindBy(id="com.royalmail.pda:id/txtvalue")
	private MobileElement deviceLoc;

	public SettingScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
	    PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}
	
	public void clickBackArrow(){
	     back.click();
		}

	public void tapOnLaunchBlutooth() throws InterruptedException{
		blueToothUtility.click();
		Thread.sleep(2000);
		driver.navigate().back();
	}
	
	public void tapOnLaunchAccessibility()throws InterruptedException{
		blueToothAccess.click();
		Thread.sleep(2000);
		driver.navigate().back();
	}
	
}
