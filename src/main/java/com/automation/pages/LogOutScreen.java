package com.automation.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.TimeOutDuration;

public class LogOutScreen extends Page{

	@AndroidFindBy(className="android.widget.Button")
	private List<MobileElement> selectOption;
	
	@AndroidFindBy(id="android:id/button1")
	private MobileElement yes;
	
	@AndroidFindBy(id="android:id/button2")
	private MobileElement no;
	
	public LogOutScreen(AppiumDriver<MobileElement> driver) {
		super(driver);
		PageFactory.initElements(new AppiumFieldDecorator(driver, new TimeOutDuration(DEFAULT_WAIT_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS)), 	this);
	}

	public void selectValue(int index){
		selectOption.get(index).click();
	}
	
	public LoginScreen selectValue(String selectValue){
       for (MobileElement loc :selectOption) {
           if (loc.getText().contains(selectValue)) {
               loc.click();
               yes.click();
               break;
           }
       }
       return new LoginScreen(driver);
   }
	
}
