package com.automation.steps;

import java.util.List;

import org.junit.Assert;

import com.automation.pages.AdhocDeliveryScreen;
import com.automation.pages.ChangeStatusScreen;
import com.automation.pages.DuplicationText;
import com.automation.pages.OutDoorScreen;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ChangeStatusSteps {

	protected AppiumDriver<MobileElement>driver;
	ChangeStatusScreen change;
	AdhocDeliveryScreen adhoc;
	DuplicationText duplicate;

	public ChangeStatusSteps (){
		this.driver=Hook.getDriver();
	}

	@Given("^the user is change status screen$")
	public void userVerifyChangeStatusScreen(){
		change = new ChangeStatusScreen(driver);
		Assert.assertTrue(change.assert_changeStatusText());

	}
	@When("^the user selects the Deliver to Neighbour$")
	public void userSelectDTN() throws InterruptedException {
		change.doSomething("Deliver to Neighbour");
	}
	
	@When("^the user verifies the Duplicate Text$")
	public void duplication() {
		//change.isDuplicateFound("Deliver to Neighbour");
		duplicate = new DuplicationText(driver);
		duplicate.duplicateText();
	}

	@When("^the user selects the Deliver to SafePlace$")
	public void userSelectDTS() throws InterruptedException{
		change.doSomething("Deliver to Safeplace");
	}
	
	@When("^the user selects the Deliver to customer$")
	public void userSelectDTC() throws InterruptedException {
		change.doSomething("Deliver to Customer");
	}

	@When("^the user selects the option Deliver to Local Collect$")
    public void userSelectDTLC() throws InterruptedException{
        change.doSomething("Deliver to Local Collect");
    }
	
	@When("^the user selects the Not Deliered$")
	public void userSelectNotDelivered() throws InterruptedException{
		change.doSomething("Not Delivered");
	}
	
	@When("^the user selects all Not Delivered list$")
	public void userSelectAllNotDeliveryList() throws InterruptedException{
		change = new ChangeStatusScreen(driver);
		change.selectAllNotDeliveredList();
		
	}
	
	@Then("^the user is on Adhoc delivery page$")
	public void clickOnContinue() throws InterruptedException{
		adhoc = new AdhocDeliveryScreen(driver);
		adhoc.clickSubmitadhoc();
	}

	@Then("^the user selects not safe$")
	public void userSelectNotSafePlace() throws InterruptedException{
		change.doSomething("Safeplace not available or not safe");
	}
	
	@Then("^user fetch all barcode options and verify barcode options does not inlcude options$")
    public void barcodeOptions(DataTable table) throws InterruptedException{
        change = new ChangeStatusScreen(driver);
        List<List<String>> data= table.raw();
        change.CapturebarcodeOptions(data.get(1).get(0));
    }
	
	@Then("^user fetch all barcode options and verify barcode options$")
	public void assertBarcodeOptions(DataTable table) throws InterruptedException{
		change = new ChangeStatusScreen(driver);
		List<List<String>> data= table.raw();
		change.CapturebarcodeOptionsandAssert(data.get(1).get(0));
	}
	
}
