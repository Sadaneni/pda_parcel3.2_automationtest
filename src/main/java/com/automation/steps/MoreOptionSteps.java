package com.automation.steps;

import com.automation.pages.MoreOptionsScreen;

import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class MoreOptionSteps {
	protected AppiumDriver<MobileElement>driver;
	
	MoreOptionsScreen more;
	
	public MoreOptionSteps(){
		this.driver= Hook.getDriver();
	}
	
	@When("^the user selects all options$")
	public void selectMoreOptions(){
		more = new MoreOptionsScreen(driver);
		more.selectAllOptions();
	}
}
