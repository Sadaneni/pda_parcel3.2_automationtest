package com.automation.steps;

import java.io.IOException;

import org.junit.Assert;

import com.automation.pages.AcceptanceScanScreen;
import com.automation.pages.AdhocDeliveryScreen;
import com.automation.pages.EnquiryOfficeScreen;
import com.automation.pages.GenericMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AcceptanceScanSteps {
	final String ACCEPTANCEBARCODE="QC005210111GB";
	final String INVALIDBARCODE="50269010220";
	protected AppiumDriver<MobileElement>driver;
	AcceptanceScanScreen screen;
	EnquiryOfficeScreen enquiry;
	AdhocDeliveryScreen ADS;
	
	public AcceptanceScanSteps(){
		this.driver=Hook.getDriver();
	}
	@Given("the user in acceptance Scan$")
	public void userVerifyAcceptanceScreen(){
		screen = new AcceptanceScanScreen(driver);
		Assert.assertTrue(screen.assert_acceptanceText());
	}
	@When("^the user send acceptance Scan Barcode$")
	public void userSendBarcode() {
		screen.sendBarcode(ACCEPTANCEBARCODE);
	} 
	
	@When("^the user sends Invalid Barcode as \"([^\"]*)\"$")
	public void userSendInvalidBarcode(String arg1) {
	//	screen = new AcceptanceScanScreen(driver);
	//	screen.sendBarcode(INVALIDBARCODE);
		 ADS = new AdhocDeliveryScreen(driver);
	        ADS.sendBarcode(arg1);
	} 
	
	@Then("^Capture Error message$")
	public void Capture_ErrMsg() throws IOException{
		
		String Errmsg = null;
		GenericMethods enquiry = new GenericMethods(driver);
		Errmsg = enquiry.CaptureErrMsg();	
		System.out.println("Captured Error Message is : " + Errmsg);
		enquiry.CaptureScreenshot("InvalidBarcode");
	}
	
	@And("^click OK button$")
	public void Click_btn() throws IOException{		
		GenericMethods btn = new GenericMethods(driver);
		btn.clickbtn();
	}
	
	
	@Then("^the user click on Submit Button$")
	public void clicOnSubmit(){
		screen = new AcceptanceScanScreen(driver);
		screen.tapOnSubmit();

	}


}
