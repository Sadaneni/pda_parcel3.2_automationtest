package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.GenericMethods;
import com.automation.pages.ManageOutDoorScreen;
import com.automation.pages.PendingScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ManageOutDoorSteps {
	final String NUMBER="0000000000";
	protected AppiumDriver<MobileElement>driver;

	ManageOutDoorScreen manage;
	GenericMethods generic;
    PendingScreen screen;
    
	public ManageOutDoorSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is on Delivery Route Page$")
	public void outDoorText(){
		manage = new ManageOutDoorScreen(driver);
		Assert.assertTrue(manage.assert_outDoorText());
	}
	@When("^the user enters Delivery Route$")
	public void userEnterDeliveryRoute() throws InterruptedException {
		manage.EnterDeliveryRoute(NUMBER);
	}
	
	@When("^the user enters Delivery Route as \"([^\"]*)\"$")
	public void userSendDeliveryRoute(String arg1) throws InterruptedException {
		manage = new ManageOutDoorScreen(driver);
		manage.EnterDeliveryRoute(arg1);
	}
	
	
	@And("^the user enters Collection Route as \"([^\"]*)\"$")
	public void EnterCollectionRoute(String arg1) throws InterruptedException
	{
		manage = new ManageOutDoorScreen(driver);
		manage.EnterCollectionRoute(arg1);		
	}

	@Then("^the user download manifest for the delivery route successfully$")
	public void tapOnDownloadRoute() throws InterruptedException{
		manage.clickDownloadRoute();
	}

	@Then("^verify route is successfully downloaded$")
	public void ClickDownloadRoute() throws InterruptedException{
		manage.clickDownloadRoute();
	}

	@Then("^user checks for collection route got successfully downloaded$")
	public void ClickRetry() throws InterruptedException{
		manage = new ManageOutDoorScreen(driver);
		manage.clickRetryButton();
	}
	
	@Then("^user clicks on continue button if route is not downloaded$")
    public void ClickContinueBtn() throws InterruptedException{
        manage = new ManageOutDoorScreen(driver);
        manage.clickContinueButton();
        generic = new GenericMethods(driver);
        generic.click_BackButton();
    }
    
    @Then("^Verify popup message after clicking Download Route$")
    public void VerifyMessage_Downloadroute() throws InterruptedException
    {
        screen = new PendingScreen(driver);
        screen.verify_DownloadCollectionroute_popuptext();    
    }
	
	
}
