package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.AcceptanceScanScreen;
import com.automation.pages.GenericMethods;
import com.automation.pages.HomePageScreen;
import com.automation.pages.LogOutScreen;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LogOutSteps {

	protected AppiumDriver<MobileElement>driver;
	GenericMethods generic;
	LogOutScreen log;
	
	public LogOutSteps(){
		this.driver= Hook.getDriver();
	}
	
	@When("^the userSwie Left$")
	public void userSwipeLeft() throws InterruptedException {
		generic = new GenericMethods(driver);
		generic.swipeLeft(800);
		
	} 
	
	@Then("^the user tap on Logout$")
	public void userTapLogOut() {
		log = new LogOutScreen(driver);
		log.selectValue("LOGOUT");
	} 
}
