package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.DescriptionOtherSafePlaceScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class DescriptionOtherSafePlaceSteps {
	final String DESCRIPTION ="Delivered";
	protected AppiumDriver<MobileElement> driver;

	DescriptionOtherSafePlaceScreen safescreen;

	public DescriptionOtherSafePlaceSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("^the user is on Description Screen$")
	public void userVerifyDescribesafePlace(){
		safescreen = new DescriptionOtherSafePlaceScreen(driver);
		Assert.assertTrue(safescreen.assert_describeText());
	}
	@When("^the user enters the description$")
	public void enterDescription() throws InterruptedException{
		safescreen.enterDescription(DESCRIPTION);
	}

	@Then("^the user is able to Tap on take photo$")
	public void userTapOnTakePhoto() throws InterruptedException {
		safescreen.tapOnTakePhoto();
	}
}
