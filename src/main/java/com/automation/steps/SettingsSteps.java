package com.automation.steps;

import com.automation.pages.HomePageScreen;
import com.automation.pages.LoginScreen;
import com.automation.pages.SettingScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SettingsSteps {
	protected AppiumDriver<MobileElement>driver;
	HomePageScreen home;
	SettingScreen setting;
	
	public SettingsSteps(){
		this.driver =Hook.getDriver();
	}
	
	@When("^the user select the settings option$")
	public void selectSettings() throws InterruptedException{
		home = new HomePageScreen(driver);
		home.doSomething("Settings");
	}
	@When("^the user click on Launch Bluetooth$")
	public void tapOnLaunchUtility() throws InterruptedException{
		setting = new SettingScreen(driver);
		setting.tapOnLaunchBlutooth();
	}
	
	@Then("^the user click on Accessibility$")
	public void tapOnAccessibility() throws InterruptedException{
		setting = new SettingScreen(driver);
		setting.tapOnLaunchAccessibility();
	}
	
}
