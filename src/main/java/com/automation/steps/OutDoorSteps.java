package com.automation.steps;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;

import com.automation.pages.OutDoorScreen;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class OutDoorSteps {

	protected AppiumDriver<MobileElement>driver;
	OutDoorScreen outdoor;

	public OutDoorSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is in Outdoor process screen$")
	public void userVerifyOutDoor(){
		outdoor = new OutDoorScreen(driver);
	}
	@When("^the user selects the Adhoc Delivery in outdoor process$")
	public void userSelectLocation() {
		outdoor = new OutDoorScreen(driver);
		outdoor.clickAdhocDelivery(0);
	}

	@When("^User selects Outdoor Process as \"([^\"]*)\"$")
	public void userSelectsProcess(String arg1) throws InterruptedException, IOException {
		outdoor = new OutDoorScreen(driver);
		outdoor.selectOutdoorprocess(arg1);
	}
	
	@When("^the user selects the change Route in outdoor process$")
	public void userSelectsProcess(){
		outdoor = new OutDoorScreen(driver);
		outdoor.selectOutdoorprocess("Change Route");
	}
	
	@When("^the user selects the cancel route in outdoor process$")
	public void userSelectsCancelRoute(){
		outdoor = new OutDoorScreen(driver);
		outdoor.selectOutdoorprocess("Cancel Route");
	}
	
	@When("^the user enter collection route for Adhoc delivery as \"([^\"]*)\"$")
	public void Enter_CollectionRoute_AdhocCollection(String arg1)
	{
		outdoor = new OutDoorScreen(driver);
		outdoor.Route_AdhocCollection(arg1);		
	}

	@Then("^User tap on Cancel button in Closing Outdoor$")
	public void clickOnCancelbtn() throws InterruptedException{
		outdoor.clickCancel();		
	}

	@Then("^user clicks on OK button for Adhoc collection route$")
	public void click_OK_Adhoccollection() throws InterruptedException{
    	outdoor = new OutDoorScreen(driver);
    	outdoor.clk_OK_AdhocCollection();	
	}
	
	@Then("^User tap on No button in Closing Outdoor$")
	public void clickOnNobtn() throws InterruptedException{
		outdoor.clickNo();		
	}

	@Then("^User tap on Yes button for confirmation$")
	public void clickOnYes() throws InterruptedException{
		outdoor.clickYes();	
	}

	@When("^User selects the Outdoor Process dropdown$")
	public void userSelectDropdoen() {
		outdoor = new OutDoorScreen(driver);
		outdoor.slctprcsDropdown();
	}    

	@And("^User should stay in Outdoor process screen$")
	public void VerifyOutdoorScreen() throws InterruptedException{
		outdoor = new OutDoorScreen(driver);
		Assert.assertTrue(outdoor.assert_outDoorText());  		
	}


	@Then("^the user is on AdhocDelivery screen$")
	public void clickOnContinue() throws InterruptedException{
		outdoor.continueAdhocScreen();
	}


	@And("^User should select the reason dropdown$")
	public void selectrsndrpdwn()
	{
		outdoor = new OutDoorScreen(driver);
		outdoor.slctNobtnDropdown();		
	}


	@And("^User should select reason value as \"([^\"]*)\"$")
	public void selectrsndrpdwn(String arg1)
	{
		outdoor = new OutDoorScreen(driver);
		outdoor.selectReasonValue(arg1);		
	}

	@Then("^user click on Submit button$")
	public void clkSbtBtn() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
		outdoor.clickSbmtbtn();
	}
	
	@Then("^User checks message on Closing Outdoor Pop up$")
	public void assert_msg_closingOutdoor(DataTable table) 
	{
		List<List<String>> data= table.raw();
		outdoor = new OutDoorScreen(driver);
		outdoor.assert_msg(data.get(1).get(0));		
	}
	
	@Then("^User click on OK button on Closing Outdoor window$")
	public void clk_OKBtn() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
		outdoor.clickYes();
	}
	
	@When("^user enter adhoc collection details$")
		public void AdhocCollectionDetails() throws InterruptedException {
	    	outdoor = new OutDoorScreen(driver);
	    	outdoor.Enter_adhoccollection_details();
			
		}    
	
	@Then("^user click on continue button$")
	public void clk_ContinueBtn() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
    	outdoor.clk_ctn();
	}
	
	
	@Then("^user select submit button$")
	public void clk_SubmitBtn_AdhocCollection() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
    	outdoor.clk_sbt();
		
	}
	
	@Then("^user select mail type to be collected$")
	public void Select_mailtype() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
    	outdoor.select_mailtype();
	}
	
	@Then("^user click on done button$")
	public void clk_done() throws InterruptedException
	{
		outdoor = new OutDoorScreen(driver);
    	outdoor.clk_dne();
	}

}
