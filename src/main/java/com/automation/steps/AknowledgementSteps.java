package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.AknowledgementScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AknowledgementSteps {
	final String SIGNATURE="RAM";
	protected AppiumDriver<MobileElement>driver;
	AknowledgementScreen know;
	
	public AknowledgementSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is in signature Acknowledgement Page$")
	public void userVerifyAcknowledgementScreen(){
		know = new AknowledgementScreen(driver);
		know.screenRotate();
		Assert.assertTrue(know.assert_nameText());
	}
	@When("^the user sign the signature$")
	public void userSignSignature() {
		Assert.assertTrue(know.assert_royalMailIcon());
		System.out.println("Sadaneni");
		know.sign();
		know.sign1();
		know.sign2();
	} 

	@Then("^click on submit Button$")
	public void clicOnSubmit() throws InterruptedException{
		know.clickDone();
	}

}
