package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.AdhocDeliveryScreen;
import com.automation.pages.GenericMethods;
import com.automation.pages.HomePageScreen;
import com.automation.pages.NeighbourDetailScreen;
import com.automation.pages.PendingScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class NeighbourDetailSteps {
	protected AppiumDriver<MobileElement>driver;

	NeighbourDetailScreen neighbour;
	PendingScreen pending;
	GenericMethods genmthd;
	
	public NeighbourDetailSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("^the user is in neighbour detilas page$")
	public void userVerifyNeighbourDetailsPage(){
		neighbour = new NeighbourDetailScreen(driver);
	}
	@And("^the user enters Neighbourname$")
	public void userVerifyAndEnterNeighbourName() {
		Assert.assertTrue(neighbour.assert_neighbourName());
		neighbour.enterNeighbourName(0);
	}

	@And("^the user enters House no$")
	public void userVerifyAndEnterHouseNo() {
		Assert.assertTrue(neighbour.assert_neighbourAddress());
		neighbour.enterNeighbourAddress(1);
	}
	@And("^the user enter the street name$")
	public void userVerifyAndEnterStreetName() {
		Assert.assertTrue(neighbour.assert_streetName());
		neighbour.enterStreetName(2);
	}
	@When("^the user tap on Done button$")
	public void clickOnTapButton() throws InterruptedException{
		neighbour = new NeighbourDetailScreen(driver);
		neighbour.tapDone();
	}

	@Then("^the user is on pending screen$")
	public void userIsOnPendingScreen(){
		pending = new PendingScreen(driver);
		Assert.assertTrue(pending.assert_pending());
	}
	@Then("^the user enter customer name$")
	public void userEnterCustomerName(){
		neighbour = new NeighbourDetailScreen(driver);
		neighbour.enterCustomerName("RMG");
	}
	
	@When("^the user is able to perform signature$")
    public void performSignature()
    {
        HomePageScreen ans = new HomePageScreen(driver);
        ans.signature();
    }
    
    @Then("^click on Submit button$")
    public void ClickSubmitButton(){
        genmthd = new GenericMethods(driver);
        genmthd.ClickDone();
    }

}
