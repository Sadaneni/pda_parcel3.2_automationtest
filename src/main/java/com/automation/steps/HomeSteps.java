package com.automation.steps;

import java.io.IOException;

import org.junit.Assert;

import com.automation.pages.GenericMethods;
import com.automation.pages.HomePageScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class HomeSteps {

	protected AppiumDriver<MobileElement>driver;
	HomePageScreen home;
	GenericMethods GenMethods;

	public HomeSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is on homepage$")
	public void userVerifyHomePage(){
		home = new HomePageScreen(driver);
		Assert.assertTrue(home.assert_homeText());
	}
	
	@When("^the user selects the home option$")
	public void userSelectsHomeOption(){
		home = new HomePageScreen(driver);
		home.clickMoreOption();
	}
	
	@When("^the user selects out door$")
	public void userSelectOutDoor() {
		home.clickOutDoor(1);
	}
	
	@When("^the user selects Home screen option as \"([^\"]*)\"$")
    public void userSelectHomeScreenOption(String arg1) {
        home.SelectHomeScreenOption1(arg1);        
    }

	@Then("^continue to manage out door screen$")
	public void continueToManageOutdoor() throws InterruptedException{
		home.continueToManageOutDoor();
	}

	@When("^the user selects Indoor$")
	public void userSelectsIndoor() {
		home = new HomePageScreen(driver);
		home.clickIndoor(0);
	}

	@Then("^continue to Indoor screen$")
	public void continueToIndoor() throws InterruptedException{
		home.continueToIndoor();
	}
	
	@Then("^Navigate to all options present on Home screen$")
    public void Clickalloption() throws InterruptedException, IOException
    {
        home.ClickAllOptions();        
        GenMethods = new GenericMethods(driver);
        GenMethods.SwipeScreenRight();;        
        home.ClickAllOptions();
        GenMethods.SwipeScreenLeft();
    }
    
    @When("^user perform swipe$")
    public void userperfromlogout() throws InterruptedException {
        
        GenericMethods genmthd = new GenericMethods(driver);
        genmthd.SwipeScreenRight();        
    }

    @Then("^user verifies Home screen options \"([^\"]*)\"$")
    public void VerifyHomeScreenOptions(String arg1) throws InterruptedException
    {
        home = new HomePageScreen(driver);
        String optns = home.getHomeScreenOptions();
        GenMethods = new GenericMethods(driver);
        GenMethods.SwipeScreenRight();            
        String optns1 = home.getHomeScreenOptions();
        GenMethods.SwipeScreenLeft();        
        optns = optns + "," + optns1;
        System.out.println("The values are : " + optns);
        Assert.assertEquals(optns, arg1);        
    }
    
    @Then("^User is on Memo Page$")
    public void AssertMemoPage()
    {
        home = new HomePageScreen(driver);
        Assert.assertTrue(home.assert_homescreenmemo());        
    }
    
    
    @And("^Click on Wite a memo option$")
    public void ClickMemoButton()
    {
        home = new HomePageScreen(driver);
        home.clickWriteamemobutton();        
    }
    
    @Then("^user send his customized message as \"([^\"]*)\"$")
    public void SendMemo(String arg1)
    {
        home = new HomePageScreen(driver);
        home.SendMemo(arg1);        
        
    }
    
    @Then("^user verifies Memo message as \"([^\"]*)\"$")
    public void VerifyMemo(String arg1)
    {
        home = new HomePageScreen(driver);
        //home.SendMemo(arg1);        
        
    }
    
    @And("^Verify message is sent successfully$")
    public void VerifyMemo()
    {
        home = new HomePageScreen(driver);
        Assert.assertTrue(home.assert_Verifymemo());
    }
    
    @Then("^the user selects Home screen option as Watch&Win, Phone & Navigation$")
    public void Selectoptions()
    {
        home.SelectHomeScreenOption1("WATCH&WIN");
        Assert.assertTrue(home.assert_VerifyHomeScreenNavigated());
        driver.navigate().back();
        home.SelectHomeScreenOption1("PHONE");
        Assert.assertTrue(home.assert_VerifyHomeScreenNavigated());
        driver.navigate().back();
        home.SelectHomeScreenOption1("NAVIGATION");
        Assert.assertTrue(home.assert_VerifyHomeScreenNavigated());
        driver.navigate().back();
    }

}
