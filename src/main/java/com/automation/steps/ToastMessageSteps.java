package com.automation.steps;

import org.testng.Assert;

import com.automation.pages.LoginScreen;
import com.automation.pages.ToastMessages;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ToastMessageSteps {
	protected AppiumDriver<MobileElement>driver;
	
	final String USERNAME ="Andrew.Scarlt2";
	final String PIN ="432";
	LoginScreen login;
	ToastMessages toast;
	
	public ToastMessageSteps(){
		this.driver = Hook.getDriver();
	}
	
	@Given("^the  user enter the invalid UserName and Password$")
	public void enterInvalidUserName(){
		login = new LoginScreen(driver);
		login.EnterUserName(USERNAME);
		login.EnterPin(PIN);
	}
	@Then("^the  user verify the toast message$")
	public void verifyToastMessage(){
		toast = new ToastMessages(driver);
	    toast.VerifyToastMessage();	
	}

}
