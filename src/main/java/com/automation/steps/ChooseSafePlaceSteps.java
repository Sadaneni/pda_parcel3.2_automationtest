package com.automation.steps;

import com.automation.pages.ChooseSafePlaceScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class ChooseSafePlaceSteps {

	protected AppiumDriver<MobileElement>driver;
	ChooseSafePlaceScreen safescreen;

	public ChooseSafePlaceSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("^the user is on choose SafePlace screen$")
	public void userVerifySafePlaceScreen(){
		safescreen= new ChooseSafePlaceScreen(driver);
	}
	@When("^the user selects the Garage$")
	public void userSelectsafeLocation() throws InterruptedException{
		safescreen.doSomething("Garage");
	}

	@When("^the user selects the Other$")
	public void userSelectOther() throws InterruptedException{
		safescreen.doSomething("Other");
	}


	@Then("^the user able to click on continue$")
	public void userTapOnContinue() {
		safescreen.tapOnContinue();
	}
}
