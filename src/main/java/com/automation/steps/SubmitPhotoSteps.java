package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.CameraScreen;
import com.automation.pages.PendingScreen;
import com.automation.pages.SubmitPhotoScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SubmitPhotoSteps {

	protected AppiumDriver<MobileElement>driver;

	CameraScreen camera;
	SubmitPhotoScreen submit;
	PendingScreen pending;

	public SubmitPhotoSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user take the safeplace photo$")
	public void userTakePicOfSafePlace() throws InterruptedException{
		camera = new CameraScreen(driver);
		camera.takePicture();
	}
	@When("^the user clik on submit photo$")
	public void userClickOnSubmitButton() throws InterruptedException {
		submit = new SubmitPhotoScreen(driver);
		submit.tapSubmitPhoto();
	}

	@When("^the user clicks on my camera is Faulty$")
	public void userClickOnFaultyCamera() {
		submit = new SubmitPhotoScreen(driver);
		submit.tapOnFaultyCamera();
	}
	
	@Then("^the user is on pending job screen$")
	public void userIsOnPendingScreen(){
		pending = new PendingScreen(driver);
		Assert.assertTrue(pending.assert_pending());

	}

}
