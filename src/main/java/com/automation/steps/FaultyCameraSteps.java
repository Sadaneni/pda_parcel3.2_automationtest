package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.FaultyCameraScreen;
import com.automation.pages.SubmitPhotoScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class FaultyCameraSteps {
	
	protected AppiumDriver<MobileElement>driver;
	
	FaultyCameraScreen fault;
	
	public FaultyCameraSteps(){
		this.driver=Hook.getDriver();
	}
	
	@And("^the user is in faulty camera screen$")
	public void userVerifyFaultyCameraScreen()  {
		fault = new FaultyCameraScreen(driver);
		Assert.assertTrue(fault.assert_cameraIcon());
	}
	
	@And("^the user tick the check box$")
	public void userClickOnCheckBox() {
		fault.clickCheckBox();
	}
	
	@Then("^the user tap on confirm and submit button$")
	public void userTapOnSubmitButton() throws InterruptedException  {
		fault.tapOnConfirmAndSubmit();
	}
	

}
