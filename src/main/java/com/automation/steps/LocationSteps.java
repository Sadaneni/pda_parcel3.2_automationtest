package com.automation.steps;

import java.io.IOException;

import org.junit.Assert;

import com.automation.pages.EnquiryOfficeScreen;
import com.automation.pages.GenericMethods;
import com.automation.pages.LocationScreen;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class LocationSteps {
	protected AppiumDriver<MobileElement>driver;

	LocationScreen location;
    GenericMethods generic;
	
	public LocationSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is on Location Page$")
	public void userVerifyLocation(){
		location = new LocationScreen(driver);
		Assert.assertTrue(location.assert_locationIcon());
	}
	@When("^the user Select the Location$")
	public void userSelectLocation() {
		location.clickTvLocation();
		location.clickLocation(1);
	}
	
	@When("^the user Select the acceptance Location$")
	public void userSelectAcceptanceLocation() {
		location = new LocationScreen(driver);
	//	location.selectLocationByText("Enfield DO");
		location.selectLocationByText("Abbey Wood DO");
	}
	
	@When("^the user selects all locations$")
	public void userSelectsAllLocations(){
		location = new LocationScreen(driver);
		location.selectAllLocations();
	}
	
	@When("^the user turn off Wifi$")
	public void userTurnOffWifi() throws IOException {
		generic = new GenericMethods(driver);
		generic.wifi_0ff();
	}

	@And("^the user Select the Location as \"([^\"]*)\"$")
    public void userSelectsLocation(String arg1) {
        //System.out.println("Value of location in Location step page is:" + arg1);
        location = new LocationScreen(driver);
        location.clickTvLocation();
        location.SelectLocation(arg1);
    }
	
	@Then("^the user is able to continue$")
	public void clickOnContinue() throws InterruptedException{
		location.clickContinueButton();
	}

	@Then("^user navigates back from current screen$")
    public void Clickbackbtn() throws InterruptedException
    {
        EnquiryOfficeScreen clk = new EnquiryOfficeScreen(driver);
        clk.clickBackButton();
    }
    
    @Then("^user navigates back from screen$")
    public void Clickbtn() throws InterruptedException
    {
        GenericMethods generic = new GenericMethods(driver);
        generic.click_BackButton();
    }


}
