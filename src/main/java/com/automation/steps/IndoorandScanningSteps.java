package com.automation.steps;

import com.automation.pages.IndoorProcessScreen;
import com.automation.pages.IndoorandScanningScreen;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class IndoorandScanningSteps {

	protected AppiumDriver<MobileElement>driver;
	
	IndoorandScanningScreen screen;
	
	
	public IndoorandScanningSteps(){
		this.driver=Hook.getDriver();
	}
	
	
	@Then("^user selects customer dropdown$")
	public void userSelectsEnquiryOffice()
	{
		screen = new IndoorandScanningScreen(driver);
		screen.clickCustomer();
	}
		
	@Then("^user selects a customer Value as \"([^\"]*)\"$")
	 public void SelectCustomerValue(String arg1)
	 {
		screen = new IndoorandScanningScreen(driver);
		screen.selectCustomer(arg1);
	 }
	 
	@Then("^user verify the scanned items are disappeared$")
	 public void Verifyscanitems()
	 {
		screen = new IndoorandScanningScreen(driver);
		boolean val = screen.assert_Scanneditems();
		if(!val)
		{
			System.out.println("The scanned item is not available as expected");
		}
		
	 }
	
	
	
	
}
