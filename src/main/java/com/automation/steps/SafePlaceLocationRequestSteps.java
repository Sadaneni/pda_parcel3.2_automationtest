package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.SafePlaceLocationRequestScreen;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class SafePlaceLocationRequestSteps {
	protected AppiumDriver<MobileElement> driver;

	SafePlaceLocationRequestScreen requestscreen;

	public SafePlaceLocationRequestSteps(){
		this.driver=Hook.getDriver();
	}

	@When("^the user is on SafePlace location request window$")
	public void userVerifySafePlaceScreen(){
		requestscreen = new SafePlaceLocationRequestScreen(driver);
		Assert.assertTrue(requestscreen.assert_safePlaceText());
	}

	@Then("^the user clicks on confirm location is safe Taken photo of item in safe place$")
	public void userclickTakePic() {
		requestscreen = new SafePlaceLocationRequestScreen(driver);
		requestscreen.tapOnCameraIcon();
	}

}
