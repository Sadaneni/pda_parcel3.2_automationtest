package com.automation.steps;


import java.io.IOException;

import org.junit.Assert;

import com.automation.pages.IndoorProcessScreen;
import com.automation.pages.IndoorScreen;
import com.automation.pages.IndoorSubProcessScreen;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class IndoorSteps {

	protected AppiumDriver<MobileElement>driver;

	IndoorScreen indoor;
	IndoorProcessScreen process;
	IndoorSubProcessScreen subprocess;

	public IndoorSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is on Indoor Screen$")
	public void userVerifyIndoorScreen(){

	}
	@When("^the user selects Enquiry office$")
	public void userSelectsEnquiryOffice() {
		process = new IndoorProcessScreen(driver);
		process.selectProcessIndex(1);

	}
	
/*	@When("^the user selects process as Enquiry Office$")
	public void userSelectsProcess() throws InterruptedException, IOException {
		process = new IndoorProcessScreen(driver);
		process.selectprocess();

	}
	*/
	@When("^the user selects process as \"([^\"]*)\"$")
    public void userSelectsProcess(String arg1) throws InterruptedException, IOException {
        process = new IndoorProcessScreen(driver);
        process.selectprocess(arg1);
    }
	
	@When("^user select sub process value as  \"([^\"]*)\"$")
    public void userSelectsSubProcess(String arg1) throws InterruptedException, IOException {
        process = new IndoorProcessScreen(driver);
        process.selectProcessByText(arg1);
	}
	
	@And("^the user select Acceptence Scan$")
	public void userSelectsAcceptenceScan() {
		subprocess = new IndoorSubProcessScreen(driver);
		subprocess.selectSubProcessByText("Acceptance Scan");
	}

	@Then("^the user able to continue AcceptenceScanScreen$")
	public void continueToAcceptenceScanScreen(){

	}
	
	@Then("^check user is displayed with many SubProcess options$")
	public void selectsAllSubprocess() throws InterruptedException, IOException{
		subprocess = new IndoorSubProcessScreen(driver);
		subprocess.selectSubprocess();
	}
	
	@Then("^check user is displayed with many DoSubProcess options$")
	public void selectsAllDoSubprocess() throws InterruptedException, IOException{
		process = new IndoorProcessScreen(driver);
		process.selectProcess();
	}
	
	@And("^Verify the list of options under Process tab as \"([^\"]*)\"$")
    public void getAllProcessList(String arg1)
    {
        System.out.println("Value of argument is " + arg1);
        process = new IndoorProcessScreen(driver);
        String prcslst = process.CaptureProcessList();
        System.out.println("Value of prcslst is " + prcslst);
        Assert.assertEquals(prcslst, arg1);
    }
    
    @Then("^Verify the list of options under SubProcess tab as \"([^\"]*)\"$")
    public void CaptureAllSubProcessOptions(String arg1)
    {
        subprocess = new IndoorSubProcessScreen(driver);
        String sbprcsoptns = subprocess.CaptureSubProcess();
        System.out.println("The values captured run-time are : " + sbprcsoptns);
        System.out.println("The Expected values are : " + sbprcsoptns);
        Assert.assertEquals(sbprcsoptns, arg1);
    }

}
