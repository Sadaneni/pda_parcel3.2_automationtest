package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.AdhocDeliveryScreen;
import com.automation.pages.LoginScreen;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;


public class LoginSteps  {
	protected AppiumDriver<MobileElement>driver;

	final String USERNAME ="Andrew.Scarlett2";
	final String PIN = "1234";
//	final String Barcode = "anna.dignam";
    final String Pin ="7861";
	LoginScreen login;
    AdhocDeliveryScreen adhoc;
    
	public LoginSteps(){
		this.driver=Hook.getDriver();	
	}

	@Given("^the user enters in with username$")
	public void enterUserName(){
		login = new LoginScreen(driver);
		login.EnterUserName(USERNAME);
	}
	
	@Given("^the user enters in with barcode username as \"([^\"]*)\"$")
    public void enterUserName_barcode(String arg1){
        adhoc = new AdhocDeliveryScreen(driver);
        adhoc.sendBarcode(arg1);
    }
	
	@When("^the user enter in with password$")
	public void enterPin() {
		login.EnterPin(PIN);
	}
	
	@When("^the user enter in with barcode password$")
    public void enterBarcodePin() {
        login = new LoginScreen(driver);
        login.EnterPin(Pin);
    }

	@Then("^the user click on submit button$")
	public void clickOnSubmit(){
		login = new LoginScreen(driver);
		login.clickLoginButton();
	}
	
	@When("^the user enters in with username as \"([^\"]*)\" and password as \"([^\"]*)\"$") 
    public void enterUsrandPswd(String arg1, String arg2)
    {
        login = new LoginScreen(driver);
        login.EnterUserName(arg1);
        login.EnterPin(arg2);
        
    }
	
	@When("^Verify user is on Login screen$")
    public void verify_loginscreen() throws InterruptedException
    {
        login = new LoginScreen(driver);
        Assert.assertTrue(login.assert_Loginscreen());
        login.SelectEnterUserName_element();
        Thread.sleep(3000);
    }
}
