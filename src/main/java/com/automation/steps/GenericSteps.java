package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.GenericMethods;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class GenericSteps{
	
	protected AppiumDriver<MobileElement>driver;
	GenericMethods generic ;
	
	
	public GenericSteps(){
		this.driver=Hook.getDriver();
	}

	
	@And("^the user Navigate back$")
	public void navigateBack() throws InterruptedException {
		generic = new GenericMethods(driver);
		generic.click_BackButton();
	}
	
	@Then("^the user click Data lost  ok Confirmation$")
	public void confirmationOK() {
		generic = new GenericMethods(driver);
		generic.clickOK();
	}
	
}
