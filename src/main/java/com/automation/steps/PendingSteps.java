package com.automation.steps;

import org.junit.Assert;

import com.automation.pages.PendingScreen;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class PendingSteps {
	protected AppiumDriver<MobileElement>driver;
	PendingScreen pending;
	public PendingSteps(){
		this.driver=Hook.getDriver();
	}

	@Given("the user is in Manifest screen displaying pending jobs$")
	public void userVerifyPendingScreen(){
		pending = new PendingScreen(driver);
		Assert.assertTrue(pending.assert_pending());
	}
	@When("^the user clicks on Manage Outdoor button$")
	public void userClickContinueManifest() {
		pending = new PendingScreen(driver);
		pending.clickManageOutDoor();
	}
	
	@When("^user selects the job from the list$")
	public void userSelectJob() throws InterruptedException {
		pending = new PendingScreen(driver);
		pending.selectJob(1);
		Thread.sleep(2000);
	}
	

	@Then("^the user continue to adhoc delivery screen$")
	public void continueToAdhocDelivery() throws InterruptedException {
		pending.continueToAdhoc();
	}
	
	@Then("^user verifies adhoc collection popup text and click OK button$")
	public void VerifyAdhocCollection_popuptext()
	{
		pending = new PendingScreen(driver);
		pending.verify_AdhocCollection_popuptext();
		pending.clickOK_AdhocCollectio_Popup();
	}
	
}
