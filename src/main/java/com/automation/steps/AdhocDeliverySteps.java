package com.automation.steps;

import java.util.List;

import org.junit.Assert;

import com.automation.pages.AdhocDeliveryScreen;
import com.automation.providers.PdaDetailList;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;

public class AdhocDeliverySteps {
	protected AppiumDriver<MobileElement>driver;
	String deviceId="354176060769866";
	final String DTNBARCODE="QC111111115GB";               //"WL901690139GB";                 //"QC111111115GB";
	final String DTNSIGNBARCODE="TP111111115GB";
	final String TWODBARCODE = "JGB.6209F20B036819400010110107700000011301115301115TPM01..QC005210099GB4…M…………………………….L147ND4B.GB";
	AdhocDeliveryScreen adhoc;

	public AdhocDeliverySteps(){
		this.driver=Hook.getDriver();
	}

	@Given("^the user is in Adhoc Delivery Screen$")
	public void userVerifyOutDoor(){
		adhoc = new AdhocDeliveryScreen(driver);
		Assert.assertTrue(adhoc.assert_customer());
		Assert.assertTrue(adhoc.assert_changeStatusText());
	}
	@When("^the user scan the Barcode$")
	public void userSendBarcode() {
		adhoc = new AdhocDeliveryScreen(driver);
		adhoc.sendBarcode(DTNBARCODE);
	}

	@When("^the use add deviceid \"([^\"]*)\"$")
	   public void getDeviceId(String _deviceid) {
	      this.deviceId = _deviceid;
	   }

	
	@When("^the user scan the Barcode as \"([^\"]*)\"$")
	public void Barcodewitharg(String arg1) {
		adhoc = new AdhocDeliveryScreen(driver);
		adhoc.sendBarcode(arg1);
	}
	
	@And("^user enter barcode manually as \"([^\"]*)\"$")
    public void enterBarcodeManually(String arg1)
    {
        adhoc = new AdhocDeliveryScreen(driver);
        adhoc.clickNumber();
        adhoc.enterBarcode(arg1);
        adhoc.clicksearchbutton();
    }
	
	@When("^the user scan the Barcode with Signature$")
	public void userSendSignatureBarcode() {
		adhoc.sendBarcode(DTNSIGNBARCODE);
	}
	
	
	@When("^the user scan the different Barcodes$")
	public void userBracode(DataTable table){
		List<List<String>> data = table.raw();
		int value = data.size(); 
		        for(int i=1;i<value;i++)
		        {
		           adhoc.sendBarcode(data.get(i).get(2));
		       }
		 	}

	@When("the user scan the different Barcodes and event codes$") 
	public void userBarcodeAndEventCode(DataTable table) {
		List<List<String>> data = table.raw();
		int value = data.size();
		for (int i = 1; i < value; i++) {
			String _barcode = data.get(i).get(2);
			String _eventCode = data.get(i).get(3);
			// send barcode
			adhoc.sendBarcode(_barcode);
			// store in-memory for CSV creation
			PdaDetailList.getInstance().addPdaDetail(_barcode, _eventCode, this.deviceId);
		}
	}
	
	@Then("^the user Barcode is read by device as selected as Deliver to Customer by default$")
	public void clickOnContinue(){
		adhoc.clickArrow();
	}

	@Then("^now the device will have Deliver to Safeplace$")
	public void clickOnArrow(){
		adhoc.clickArrow();
	}

	@Then("^the user tap on DTSP Barcode$")
	public void tapOnBarcode(){
		adhoc.selectBarcode(1);
	}
	
	@Then("^the user want apply all of items$")
	public void selectAppliesAllOk(){
		adhoc = new AdhocDeliveryScreen(driver);
		adhoc.selectOK();
	}
	
	@Then("^click on submit button in JobDetails page$")
	public void clkSubmit_JobDetails(){
		adhoc = new AdhocDeliveryScreen(driver);
		adhoc.clickSubmit_jobdetails();
	}


}
