package com.automation.steps;

import com.automation.providers.AppiumConfigurationProvider;
import com.automation.providers.PdaDetailList;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Hook {

    protected static AppiumDriver<MobileElement> driver = null;

    /**
     * Return current instance of appium driver
     */
    public static AppiumDriver<MobileElement> getDriver() {
        return driver;
    }

    @Before
    public void setUp() throws IOException {
        String command = "adb shell am start -n io.appium.unlock/.Unlock";
        Runtime.getRuntime().exec(command);
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME, AppiumConfigurationProvider.platformName());
        cap.setCapability(MobileCapabilityType.DEVICE_NAME, AppiumConfigurationProvider.deviceName());
     // If you want the framework to install the app on each run then uncomment the below
        //cap.setCapability(MobileCapabilityType.APP, AppiumConfigurationProvider.appAbsolutePath());
        
        //If you uncomment the above please COMMENT OUT the below 2 lines
        cap.setCapability(MobileCapabilityType.NO_RESET, true);
        cap.setCapability(MobileCapabilityType.FULL_RESET, false);
        cap.getCapability(MobileCapabilityType.PLATFORM_NAME).equals(MobilePlatform.ANDROID);  // platform is android
        cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, AppiumConfigurationProvider.appPackage());
        cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, AppiumConfigurationProvider.appActivity());
        cap.setCapability(AndroidMobileCapabilityType.SUPPORTS_APPLICATION_CACHE, "false");
        driver = new AndroidDriver<MobileElement>(new URL(AppiumConfigurationProvider.url()), cap);

        // set implicit timeout for @FindBy
        driver.manage().timeouts().implicitlyWait(AppiumConfigurationProvider.implicitTimeout(), TimeUnit.SECONDS);

        //
        PdaDetailList.getInstance();
    }

    /**
     * This method is executed whenever a  test is completed
     */
    @After
    public void tearDown(Scenario scenario) {
        //	 if (scenario.isFailed()) {
        scenario.embed(((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES), "image/png");
        //       }
        if (driver != null) {
            driver.resetApp();
            driver.closeApp();
            driver.quit();
        }

        PdaDetailList.getInstance().writeCsvValues();
    }


}

