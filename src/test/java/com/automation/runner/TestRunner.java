package com.automation.runner;


import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.automation.steps.AppiumServerController;

//import com.github.mkolisnyk.cucumber.runner.ExtendedCucumber;
//import com.github.mkolisnyk.cucumber.runner.ExtendedCucumberOptions;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)

@CucumberOptions(strict = false, features = "Features", tags={"@DelivertoSafeplacewithOther,@NoSafePlaceAvailable,@DelivertoSafeplacewithGarage,@FaultyCamera,@DTNWithOutSignature,@DTNSubmitSignature,@AcceptenceScan,@ToastMessages,@ProductDataRegression"},
format = { "pretty",
"json:target/cucumber.json",
"html:target/cucumber-html-report",
"com.cucumber.listener.ExtentCucumberFormatter:target/report.html",}, glue = { "com.automation.steps" })

public class TestRunner {
/*
	@BeforeClass ,json:target/cucumber.json
	public static void startAppium(){
		AppiumServerController server = new AppiumServerController();
	    server.startServer();
	}
	
	@AfterClass
	public static void stopAppium() throws InterruptedException{
		AppiumServerController server = new AppiumServerController();
		server.stopServer();
	}*/
}
//@DelivertoSafeplacewithOther,@NoSafePlaceAvailable,@DelivertoSafeplacewithGarage,@FaultyCamera,@DTNWithOutSignature,@DTNSubmitSignature,@AcceptenceScan,@ToastMessages,@ProductDataRegression

//@NoSafePlaceAvailable
//@DelivertoSafeplacewithOther 
//@DelivertoSafeplacewithGarage
//@FaultyCamera 
//@AcceptenceScan
//@DTNWithOutSignature
//@DTNSubmitSignature 
//@ToastMessages
//@DifferentDeliveryRoutes
//@ProductDataRegression